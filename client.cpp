#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <iostream>
#include "choose_server_window.h"
#include <QApplication>



#ifdef _WIN32
//////////////////////////////////////////////////////
////////////////////////////////LEAK DETECTOR
#include <vld.h>
///////////////////////////////////////////////////////
#endif

using namespace std;

int  main(int argc, char **argv) {
	QApplication application(argc, argv);
	ChooseServerWindow window;
	window.setWindowTitle("Welcome");
	window.show();
	return application.exec();
}