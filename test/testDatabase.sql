﻿-- Sequence: users_seq

-- DROP SEQUENCE users_seq;

CREATE SEQUENCE users_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE users_seq
  OWNER TO backuper;

-- Sequence: files_seq

-- DROP SEQUENCE files_seq;

CREATE SEQUENCE files_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE files_seq
  OWNER TO backuper;

-- Sequence: catalogs_seq

-- DROP SEQUENCE catalogs_seq;

CREATE SEQUENCE catalogs_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE catalogs_seq
  OWNER TO backuper;




-- Create tables section -------------------------------------------------

-- Table users

CREATE TABLE "users"(
 "id" Bigint NOT NULL DEFAULT nextval('users_seq'::regclass),
 "login" Character varying(30) NOT NULL,
 "passwd" Character (32) NOT NULL,
 "root" Bigint
)
;

-- Create indexes for table users

CREATE INDEX "IX_main_catalog" ON "users" ("root")
;

CREATE UNIQUE INDEX "unique_login" ON "users" ("login")
;

-- Add keys for table users

ALTER TABLE "users" ADD CONSTRAINT "Unique_Identifier1" PRIMARY KEY ("id")
;

-- Table files

CREATE TABLE "files"(
 "id" Bigint NOT NULL DEFAULT nextval('files_seq'::regclass),
 "name" Character varying(100) NOT NULL,
 "in_filesystem_name" Character varying(32) NOT NULL,
 "version" Bigint NOT NULL,
 "owner" Bigint NOT NULL,
 "parent" Bigint NOT NULL
)
;

-- Create indexes for table files

CREATE INDEX "IX_version" ON "files" ("version")
;

CREATE INDEX "IX_has_file" ON "files" ("owner")
;

CREATE INDEX "IX_contains" ON "files" ("parent")
;


-- Add keys for table files

ALTER TABLE "files" ADD CONSTRAINT "Unique_Identifier2" PRIMARY KEY ("id")
;

-- Table catalogs

CREATE TABLE "catalogs"(
 "id" Bigint NOT NULL DEFAULT nextval('catalogs_seq'::regclass),
 "name" Character varying(35) NOT NULL,
 "parent" Bigint,
 "owner" Bigint
)
;

-- Create indexes for table catalogs

CREATE INDEX "IX_contains_catalog" ON "catalogs" ("parent")
;

CREATE INDEX "IX_has_cataloge" ON "catalogs" ("owner")
;

-- Add keys for table catalogs

ALTER TABLE "catalogs" ADD CONSTRAINT "Unique_Identifier3" PRIMARY KEY ("id")
;

-- Create relationships section ------------------------------------------------- 

ALTER TABLE "catalogs" ADD CONSTRAINT "contains_catalog" FOREIGN KEY ("parent") REFERENCES "catalogs" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "files" ADD CONSTRAINT "version" FOREIGN KEY ("version") REFERENCES "files" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "files" ADD CONSTRAINT "has_file" FOREIGN KEY ("owner") REFERENCES "users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "catalogs" ADD CONSTRAINT "has_cataloge" FOREIGN KEY ("owner") REFERENCES "users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "files" ADD CONSTRAINT "contains" FOREIGN KEY ("parent") REFERENCES "catalogs" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "users" ADD CONSTRAINT "main_catalog" FOREIGN KEY ("root") REFERENCES "catalogs" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION
;
-------------MASTER PROPERTIES---------------------
INSERT INTO users(
            login, passwd)
    VALUES ('admin', '912ec803b2ce49e4a541068d495ab570');

INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('rootMaster', 1, 1);

UPDATE users
   SET  root=1
 WHERE id=1;
------------------------------------------------------


-------------USERS INSERT-----------------------------
INSERT INTO users(
            login, passwd)
    VALUES ('user2', '7e58d63b60197ceb55a1c487989a3720');

INSERT INTO users(
            login, passwd)
    VALUES ('user3', '92877af70a45fd6a2ed7fe81e1236b78');

INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('2', 1, 2);
INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('3', 1, 3);

 UPDATE users
   SET  root=2
 WHERE id=2;
 UPDATE users
   SET  root=3
 WHERE id=3;

--------------------------------------------------------

-------------CATALOGS INSERT----------------------------

INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('obrazy', 2, 2);

INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('muzyka', 2, 2);

INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('obrazy', 3, 3);
INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('muzyka', 3, 3);
    
INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('muzyka folkowa', 6, 3);

INSERT INTO catalogs(
            name, parent, owner)
    VALUES ('rozne', 2, 2);

---------------------------------------------------------


-------------FILES INSERT--------------------------------
INSERT INTO files(
            name, in_filesystem_name, version,owner, 
            parent)
    VALUES ('utwor1 u 2','1',1, 2, 5);

INSERT INTO files(
            name, in_filesystem_name, version,owner, 
            parent)
    VALUES ('utwor u 3','2',2 , 3, 7);

INSERT INTO files(
            name, in_filesystem_name,version, owner, 
            parent)
    VALUES ('obraz 1 u2','3', 3 ,2, 4);

INSERT INTO files(
            name, in_filesystem_name,version, owner, 
            parent)
    VALUES ('obraz 1 u3','4', 4, 3, 6);

INSERT INTO files(
            name, in_filesystem_name,version, owner, 
            parent)
    VALUES ('folkowy utwor', '5', 5, 3, 8);

-----VERSION-----
INSERT INTO files(
            name,  in_filesystem_name, version, owner, 
            parent)
    VALUES ('chomik', '6', 6, 2, 9);

INSERT INTO files(
            name,  in_filesystem_name, version, owner, 
            parent)
    VALUES ('chomik', '7', 7, 2, 9);

INSERT INTO files(
            name,  in_filesystem_name, version, owner, 
            parent)
    VALUES ('chomik', '8', 8, 2, 9);

 UPDATE files
   SET  version=8
 WHERE id=6 OR id=7;

---------------------------------------------------------

