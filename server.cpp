#include "soci.h"
#include <postgresql/soci-postgresql.h>
#include "log4cpp/Category.hh"
#include <log4cpp/PropertyConfigurator.hh>
#include "log4cpp/Appender.hh"
#include "log4cpp/FileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/BasicLayout.hh"
#include "log4cpp/Priority.hh"
#include "log4cpp/Priority.hh"
#include <utility>
#include <algorithm>
#include <string>
#include <fstream>
#include <iostream>
#include <user_service.h>
#include "controller.h"
#ifdef _WIN32
//////////////////////////////////////////////////////
////////////////////////////////LEAK DETECTOR
#include <vld.h>
///////////////////////////////////////////////////////
#endif


using namespace std;


#include "config.h"

#include <string>
#include <file_service.h>
#include <vector>
#include <catalog_service.h>
#include <client_decision.h>
#include <utility>
#include <algorithm>
#include <exception/clientdecision_exception.h>
#include <exception/validateuser_exception.h>
#include <md5.h>

/*
std::string common::Config::dbPath_;
std::string common::Config::locationMasterRoot_;
int common::Config::poolSize_;
std::string common::Config::pathSeparator_;

*/


typedef pair<ClientDecision, common::File> actionOnFile;
typedef pair<ClientDecision, common::Catalog> actionOnCatalog;

int main(int argc, char **argv) {


	std::string initFileName = "log4cpp.properties";
	try {
		log4cpp::PropertyConfigurator::configure(initFileName);
	} catch (log4cpp::ConfigureFailure c) {
		std::string initFileName = "../log4cpp.properties";
		log4cpp::PropertyConfigurator::configure(initFileName);
	}

	common::Config::prepareConfiguration("./config.txt");


	Controller c;
	c.listenForConnections();




	//system("pause");
	log4cpp::Category::shutdown();
	return 0;
}
