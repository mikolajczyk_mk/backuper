#include "receiver.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include "protocol.h"


#ifdef _WIN32
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <windows.h>
	#pragma comment(lib, "Ws2_32.lib")
#elif __linux__
	#include <zconf.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
#endif

#define DEFAULT_BUFLEN 512

bool Receiver::receiveFile(){
	//FILE *file_handle = fopen("test.txt", "wb");
	if (file_handle_ != NULL)
	{
		bool ok = readFile();

		if (ok)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

bool Receiver::readFile()
{
	long unsigned filesize;
	if (!readLong(&filesize))
		return false;
	std::cout << "file size server: " << filesize << std::endl;
	if (filesize > 0)
	{
		char buffer[1024];
		do
		{
#ifdef _WIN32
			long num = min(filesize, sizeof(buffer));
#elif __linux__
			long num = std::min(filesize, sizeof(buffer));
#endif
			
			if (!readData(buffer, num))
				return false;
			int offset = 0;
			do
			{
				size_t written = fwrite(&buffer[offset], 1, num - offset, file_handle_);
				if (written < 1)
					return false;
				offset += written;
			} while (offset < num);
			filesize -= num;
			std::cout << "file size server: " << filesize << std::endl;
		} while (filesize > 0);

	}
	return true;
}

bool Receiver::readData(void *buf, int buflen)
{
	char *pbuf = (char *)buf;

	while (buflen > 0)
	{
		
		int num = recv(*socket_, pbuf, buflen, 0);
		
		if (num == 0)
		{
			return false;
		}
		else if (num == 0)
			return false;

		pbuf += num;
		buflen -= num;
	}

	return true;
}

bool Receiver::readLong(unsigned long *value){
	std::cout << "waiting for file size" << std::endl;
	if (!readData(value, sizeof(*value)))
		return false;
	*value = ntohl(*value);
	std::cout << "file size: " << *value << std::endl;
	return true;
}

bool Receiver::readLongLong(long long *value){
	if (!readData(value, sizeof(*value)))
		return false;
	*value = ntohl(*value);
	return true;
}