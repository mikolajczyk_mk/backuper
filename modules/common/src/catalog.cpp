//
// Created by ruda on 27.05.16.
//


#include "catalog.h"
#include "file_orm.hpp"
namespace common {


	long long int Catalog::getId_() const {
		return id_;
	}

	void Catalog::setId_(long long int id_) {
		Catalog::id_ = id_;
	}

	const std::string &Catalog::getName_() const {
		return name_;
	}

	void Catalog::setName_(const std::string &name_) {
		Catalog::name_ = name_;
	}



	long long int Catalog::getParent_() const {
		return parent_;
	}

	void Catalog::setParent_(long long int parent_) {
		Catalog::parent_ = parent_;
	}

	long long int Catalog::getOwner_() const {
		return owner_;
	}

	void Catalog::setOwner_(long long int owner_) {
		Catalog::owner_ = owner_;
	}

	std::string Catalog::catalog_to_string() {
		return "Catalog prameters: Id: " + std::to_string(id_) + ". Name: " + name_ + ". Parent " + std::to_string(parent_) + ". Owner " +
			   std::to_string(owner_);
	}


}