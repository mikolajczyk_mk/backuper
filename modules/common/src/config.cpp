//
// Created by ruda on 29.05.16.
//

#include <string>
#include <fstream>
#include "config.h"
std::string common::Config::dbPath_;
std::string common::Config::locationMasterRoot_;
int common::Config::poolSize_;
std::string common::Config::pathSeparator_;

namespace common {

	void Config::prepareConfiguration(std::string configuration_path) {
		std::string db_path;

		std::fstream config;
		log4cpp::Category &logger_ = log4cpp::Category::getInstance(std::string("server.db"));
		config.open(configuration_path, std::ios::in);
		if (!config) {
			logger_.info("Problem with config file");
		}
		getline(config, dbPath_);
		getline(config, locationMasterRoot_);
		std::string temp;
		getline(config, temp);
		poolSize_ = std::stoi(temp);
		getline(config, pathSeparator_);

		logger_.info("Content of config file %s.", dbPath_.c_str());
		logger_.info("Location of storage place: %s.", locationMasterRoot_.c_str());
		logger_.info("Pool size is %d. ", poolSize_);
		logger_.info("Path separator is %s.", pathSeparator_.c_str());
	};

	const std::string &Config::getDbPath_() {
		return dbPath_;
	}

	const std::string &Config::getLocationMasterRoot_() {
		return locationMasterRoot_;
	}

	int Config::getPoolSize_() {
		return poolSize_;
	}

	const std::string &Config::getPathSeparator_() {
		return pathSeparator_;
	}

}




