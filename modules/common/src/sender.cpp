#include "receiver.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include "sender.h"


#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#pragma comment(lib, "Ws2_32.lib")
//void closeSocket(int socket) { closesocket(socket); }
#elif __linux__
#include <zconf.h>
#include <sys/socket.h>
#include <netinet/in.h>
#endif

#define DEFAULT_BUFLEN 512



bool Sender::sendFile(){
	fseek(file_handle_, 0, SEEK_END);
	unsigned long filesize = ftell(file_handle_);
	rewind(file_handle_);
	if (filesize == EOF)
		return false;
	if (!sendLong(filesize))
		return false;
	std::cout << "filesize: " << filesize << std::endl;
	if (filesize > 0)
	{
		char buffer[1024];
		size_t num;
		do
		{

#ifdef _WIN32
			num = min(filesize, sizeof(buffer));
#elif __linux__
			num = std::min(filesize, sizeof(buffer));
#endif
			
			num = fread(buffer, 1, num, file_handle_);
			if (num < 1)
				return false;
			if (!sendData(buffer, num))
				return false;
			filesize -= num;
			std::cout << "filesize: " << filesize << std::endl;
		} while (filesize > 0);
		std::cout << "end" << std::endl;
	}
	return true;
}

bool Sender::sendData(void *buf, int buf_len){
	char *pbuf = (char *)buf;

	while (buf_len > 0)
	{
		int num = send(*socket_, pbuf, buf_len, 0);
		if (num == 0)
		{
			return false;
		}

		pbuf += num;
		buf_len -= num;
	}

	return true;
}

bool Sender::sendLong(long value){
	value = htonl(value);
	return sendData(&value, sizeof(value));
}

bool Sender::sendLongLong(long long value){
	value = htonl(value);
	return sendData(&value, sizeof(value));
}