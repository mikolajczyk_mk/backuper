//
// Created by ruda on 17.05.16.
//
#include "user.h"


namespace common {
	long long int  User::getId_() {
		return id_;
	}

	void  User::setId_(long long int id_) {
		User::id_ = id_;
	}

	const std::string &User::getLogin_() const {
		return login_;
	}

	void  User::setLogin_(const std::string &login_) {
		User::login_ = login_;
	}

	const std::string &User::getPasswd_() const {
		return passwd_;
	}

	void  User::setPasswd_(const std::string &passwd_) {
		User::passwd_ = passwd_;
	}

	long long int  User::getRoot_() const {
		return root_;
	}

	void  User::setRoot_(long long int root_) {
		User::root_ = root_;
	}

	std::string User::user_to_string() {
		return "User prameters: Id: " + std::to_string(id_) + ". Login: " + login_ + ". Password " + passwd_ + ". Root " +
			   std::to_string(root_);
	}


}