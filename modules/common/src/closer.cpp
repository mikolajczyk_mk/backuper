//
// Created by krzysiek on 02.06.16.
//

#include "closer.hpp"

#ifdef _WIN32
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <windows.h>
	#pragma comment(lib, "Ws2_32.lib")
#elif __linux__
	#include <zconf.h>
#endif

void Closer::closeSocket(int socket) {
#ifdef _WIN32
    closesocket(socket);
#elif __linux__
    close(socket);
#endif
}