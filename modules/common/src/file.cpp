//
// Created by ruda on 27.05.16.
//

#include "file.h"
#include "file_orm.hpp"


namespace common {


	long long int File::getId_() {
		return id_;
	}

	void File::setId_(long long id_) {
		File::id_ = id_;
	}

	const std::string &File::getName_() const {
		return name_;
	}

	void File::setName_(const std::string &name_) {
		File::name_ = name_;
	}

/*	const tm &File::getCreationDate_() const {
		return creationDate_;
	}

	void File::setCreationDate_(const tm &creationDate_) {
		File::creationDate_ = creationDate_;
	}*/

	long long int File::getSize_() const {
		return size_;
	}

	void File::setSize_(long long int size_) {
		File::size_ = size_;
	}

	const std::string &File::getFilesystemName_() const {
		return filesystemName_;
	}

	void File::setFilesystemName_(const std::string &filesystemName_) {
		File::filesystemName_ = filesystemName_;
	}

	long long File::getVersion_() const {
		return version_;
	}

	void File::setVersion_(long long version_) {
		File::version_ = version_;
	}

	long long  File::getParent_() const {
		return parent_;
	}

	void File::setParent_(long long parent_) {
		File::parent_ = parent_;
	}

	long long File::getOwner_() const {
		return owner_;
	}

	void File::setOwner_(long long owner_) {
		File::owner_ = owner_;
	}


	std::string File::getLastWriteTime_() const {
		return lastWriteTime_;
	}

	int File::getNumberOfVersion_() const {
		return numberOfVersion_;
	}

	void File::setNumberOfVersion_(int numberOfVersion_) {
		File::numberOfVersion_ = numberOfVersion_;
	}


	std::string File::file_to_string() {
		//std::string pom =
		return "File prameters: Id: " + std::to_string(id_) + ". Name: " + name_ + ". Parent " + std::to_string(parent_) + ". Owner " +
			   std::to_string(owner_);
	}

	void File::setLastWriteTime_(const std::string &lastWriteTime_) {
		File::lastWriteTime_ = lastWriteTime_;
	}


}
