//
// Created by ruda on 06.06.16.
//

#include "exception/registration_exception.h"

namespace common {
	namespace exception {
		std::string RegistrationException::msg_ = "Login alreadty exists.";

		const char *RegistrationException::what() {
			return msg_.c_str();
		}

	}
}