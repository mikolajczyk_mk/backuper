/**
* @file protocol.h
* @brief Typ wyliczeniowy odzwierciedlający protokół komunikacyjny.
* @author Krzysztof Kachniarz
*/

enum Protocol  
{
	KEEP_ALIVE = 0,
	LOGIN = 1,
	REGISTER = 2,
	SHOW_FILES = 3,
	UPLOAD_FILE = 4,
	DOWNLOAD_FILE = 5,
	ACK = 6,
	SEND_LOGIN = 7,
	SEND_PASSWORD = 8,
	NACK = 9,
	SEND_FILE_NAME = 10,
	SEND_CHECK_SUM = 11,
	UPLOAD_DIRECTORY = 12,
	SHOW_DIRECTORIES = 13,
	DOWNLOAD_CATALOG = 14,
	REMOVE_CATALOG = 15,
	REMOVE_FILE = 16
};