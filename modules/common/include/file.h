/**
* @file file.h
* @brief Plik nagłówkowy klasy File.
* @author Marta Mikołajczyk
*/

#ifndef BACKUPER_FILE_H
#define BACKUPER_FILE_H
#define NOMINMAX

#include <string>
#include <ctime>


#include "soci.h"
#include <postgresql/soci-postgresql.h>
#include <log4cpp/Category.hh>


namespace common {
/**Klasa File reprezentuje pliki użytkowika. Zawiera potrzebne zmienne oraz funkcje do operacji na obiekcie file zarówno po stronie klienta jak i serwera.*/
	class File {
	public:
/** Deklaracja przyjaźni potrzebna do operacji na bazie danych.*/
		friend class soci::type_conversion<File>;
/** Argumentowy konstruktor pliku.
 * @param name_ nazwa pliku
 * @param parent_ id katalogu w którym znajduje się plik
 * @param owner_ id właściciela pliku
 * */
		File(const std::string &name_, long long int parent_, long long int owner_) : name_(name_), parent_(parent_), owner_(owner_) { };
/** Argumentowy konstruktor pliku.
 * @param id_ id pliku
 * @param owner_ id właściciela pliku
 * */
		File(long long int id_,long long int owner_) : id_(id_) ,owner_(owner_){ };
/** Argumentowy konstruktor pliku.
 * @param id_ id pliku
 * */
		File(long long int id_) : id_(id_) { };

/** Argumentowy konstruktor pliku.
 * @param name_ nazwa pliku
 * */
		File(const std::string &name_) : name_(name_) { }
/** Konstruktor bezargumentowy. */
		File() { };
/**Metoda służąca do stworzenia obiektu string zawierającego informacje o podstawowych polach klasy. */
		std::string file_to_string();


		/*Getters and Setters*/
		void setOwner_(long long owner_);

		long long getOwner_() const;

		void setParent_(long long parent_);

		long long getParent_() const;

		void setVersion_(long long version_);

		long long getVersion_() const;

		void setFilesystemName_(const std::string &filesystemName_);

		const std::string &getFilesystemName_() const;

		void setName_(const std::string &name_);

		const std::string &getName_() const;

		void setId_(long long id_);

		long long int getId_();

		void setSize_(long long int size_);

		long long int getSize_() const;

		std::string getLastWriteTime_() const;
		void setLastWriteTime_(const std::string &lastWriteTime_);

		int getNumberOfVersion_() const;

		void setNumberOfVersion_(int numberOfVersion_) ;

	private:
		//Base information
		long long id_; /**< Unikalne id pliku. */
		std::string name_; /**< Nazwa pliku wyświetlana użytkownikowi*/
		std::string filesystemName_; /**<  Nazwa pliku pod jaką został zapisany po stronie serwera. Na chwilę obecną jest to id pliku.*/
		long long version_;/**<  Id ostatniej wersji pliku*/
		long long parent_; /**<  Id katalog w którym umieścił go użytkownik*/
		long long owner_; /**<  Id użytkownika do którego należey plik*/


		//Additional information
		std::string lastWriteTime_; /**<  Data ostatniej modyfikacji*/
		long long int size_; /**< Rozmiar pliku.*/
		int numberOfVersion_;/**<  Liczba wersji pliku.*/

		log4cpp::Category &logger_ = log4cpp::Category::getInstance(std::string("server.common")); /**<  Zmienna pomocnicza służąca do logowania zdarzeń*/

	};
}
#endif //BACKUPER_FILE_H
