/**
* @file config.h
* @brief Plik nagłówkowy klasy Config.
* @author Marta Mikołajczyk
*/
#ifndef BACKUPER_CONFIG_H
#define BACKUPER_CONFIG_H


#include <string>
#include <log4cpp/Category.hh>


namespace common {
/** Klasa służąca do udostępniania obiektom programu potrzebnych informacji jak: dane do bazy danych, rozmiar puli połączeń, lokalizacja głównego
 * katalogu programu i speparatora ścieżek. Zajmuje się parsowaniem ich z pliku (podanego w głównym pliku programu, czyli server.cpp). Bardzo ważna jest kolejność podawanych
 * agumentów, ponieważ metoda nie jest odporna na zmianę kolejności czy błędy.*/
	class Config {
	public:
/** Metoda parsująca dane z pliku na pola klasy.
 * @param configuration_path ścieżka dostępu do pliku konfiguracyjnego*/
		static void prepareConfiguration(std::string configuration_path);

		static int getPoolSize_();

		static const std::string &getDbPath_();

		static const std::string &getLocationMasterRoot_();

		static const std::string &getPathSeparator_();

	private:
		static std::string dbPath_; /**< Określa obiekt string zawierający potrzebne dane do zalogowania do bazy danych*/
		static std::string locationMasterRoot_;/**< Określa położenie główngo folderu programu. W nim będą tworzone foldery użytkownika oraz umieszczane przesyłane pliki*/
		static int poolSize_;/**< Określa pulę połączeń do bazy danych*/
		static std::string pathSeparator_; /**< Separator kolejnych członów ścieżki plików, np. pod linuksem '/' */


	};
}

#endif //BACKUPER_CONFIG_H
