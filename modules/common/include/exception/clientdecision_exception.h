/**
* @file clientdecision_exception.h
* @brief Plik nagłówkowy klasy ClientDecisionException.
* @author Marta Mikołajczyk
*/

#ifndef BACKUPER_CLIENTDECISION_EXCEPTION_H
#define BACKUPER_CLIENTDECISION_EXCEPTION_H


#include <exception>
#include <string>

namespace common {
	namespace exception {
		/** Klasa dziedziecząca po std::exception służąca do komunikowania błędnego typu działania wskazanego przez klienta dla zadanej funcji.*/
		class ClientDecisionException : std::exception {
		public:
/** Argumentowy konstruktor wyjątku.
 * @param msg_ dozwolone paramtery dla danej funkcji
 * */
			ClientDecisionException(std::string msg): msg_("Bad parameter. They are only allowed: "+ msg){};
/** Metoda odziedziczona po std::exception zwracajaca wyjaśnienie.*/

			const char *what();

		private:
			std::string msg_; /**< Wiadomość wysyłana w wyjątku*/
		};

	}
}
#endif //BACKUPER_CLIENTDECISION_EXCEPTION_H
