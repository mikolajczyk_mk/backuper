//
// Created by ruda on 06.06.16.
//

#ifndef BACKUPER_REGISTRATION_EXCEPTION_H
#define BACKUPER_REGISTRATION_EXCEPTION_H


#include <exception>
#include <string>

namespace common {
	namespace exception{
/** Klasa dziedziecząca po std::exception służąca do komunikowania, że dany login jest już zajety.*/
			class RegistrationException : std::exception {
			public:
/** Metoda odziedziczona po std::exception zwracajaca wyjaśnienie.*/
				const char* what();
			private:
				static std::string msg_;/**< Wiadomość wysyłana w wyjątku*/
			};
	}
}

#endif //BACKUPER_REGISTRATION_EXCEPTION_H
