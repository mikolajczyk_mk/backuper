//
// Created by ruda on 06.06.16.
//

#ifndef BACKUPER_VALIDATEUSER_EXCEPTION_H
#define BACKUPER_VALIDATEUSER_EXCEPTION_H


#include <exception>
#include <string>

namespace common {
	namespace exception {
/** Klasa dziedziecząca po std::exception służąca do komunikowania błędnych danych podanych przez użytkownika do zalogowania.*/
		class ValidateUserException : std::exception {
		public:
/** Metoda odziedziczona po std::exception zwracajaca wyjaśnienie.*/
			const char *what();

		private:
			static std::string msg_;/**< Wiadomość wysyłana w wyjątku*/
		};
	}
}


#endif //BACKUPER_VALIDATEUSER_EXCEPTION_H
