/**
* @file client_decision.h
* @brief Plik nagłówkowy enuma ClientDecision.
* @author Marta Mikołajczyk
*/
#ifndef BACKUPER_CLIENT_DECISION_H
#define BACKUPER_CLIENT_DECISION_H
/**
 * Enum określający decyzje klienta co zrobić z zadanym bytem.
*/
enum ClientDecision {
	ADD = 0,/**< Dodanie pliku i katalogu, kiedy na pewno nie ma takiego w bazie danych.*/
	VERSION = 1, /**< Tylko do pliku. Dodanie nowej wersji pliku.*/
	REPLACE = 2, /**< Zmiana wersji pliku bądź katalogu, w przypadku innych wersji pliku kasuje je, a w przypadku katalogu usuwane będą wszyskie znajdujące się tam pliki.*/
	ONE = 3, /**<Do usuwania pliku. Usuwa pojedyńczy plik - bez wersji.*/
	ALL = 4 /**<Usuwa plik i wszyskie jego wersje.*/
};
#endif //BACKUPER_CLIENT_DECISION_H
