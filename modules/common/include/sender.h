/**
* @file sender.h
* @brief Plik nag��wkowy klasy Sender obs�uguj�cej w�a�ciwe wysy�anie plik�w i liczb.
* @author Krzysztof Kachniarz
*/
#ifndef SENDER_H
#define SENDER_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>

/**Klasa odpowiedzialna za wysy�anie plik�w.*/
class Sender {
public:
	/**Konstruktor klasy Sender.
	* @param file_handle Wska�nik na obiekt klasy FILE wskazuj�cy na plik przeznaczony do wys�ania.
	* @param socket Gniazdko, przez kt�re b�dzie si� odbywa� transmisja.
	*/
	Sender(FILE *file_handle, int *socket) : file_handle_(file_handle), socket_(socket){};
	/**Konstruktor klasy Sender.
	* @param socket Gniazko, przez kt�re b�dzie si� odbywa� transmisja danych.
	*/
	Sender(int *socket) : socket_(socket){};
	/**Metoda inicjuj�ca wysy�anie pliku.*/
	bool sendFile();
	/**Metoda wysy�aj�ca w odpowiedni spos�b warto�� typu long long.
	* @param value Warto��, kt�ra b�dzie przes�ana.
	*/
	bool sendLongLong(long long value);
private:
	/**Metoda wysy�aj�ca okre�lony bufor danych przez gniazdko.
	* @param buf Bufor przeznaczony do wys�ania.
	* @param buf_len D�ugo�� bufora.
	*/
	bool sendData(void *buf, int buf_len);
	/**Metoda wysy�aj�ca w odpowiedni spos�b warto�� typu long - rozmiar pliku.
	* @param value Warto��, kt�ra b�dzie przes�ana.
	*/
	bool sendLong(long value);

	int *socket_;
	FILE *file_handle_;
};

#endif //SENDER_H