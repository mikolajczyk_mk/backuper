/**
* @file user.h
* @brief Plik nagłówkowy klasy User.
* @author Marta Mikołajczyk
*/
#ifndef BACKUPER_USER_H
#define BACKUPER_USER_H
#include <string>
#include "soci.h"

namespace common {
	/**Klasa User reprezentuje użytkowika. Zawiera potrzebne zmienne oraz funkcje do operacji na obiekcie user zarówno po stronie klienta jak i serwera.*/
	class User {
	public:
/** Deklaracja przyjaźni potrzebna do operacji na bazie danych.*/
		friend class soci::type_conversion<User>;
/** Argumentowy konstruktor użytkownika.
 * @param login_ login
 * @param passwd_ hasło
 * @param root_ folder główny
 * */
		User(const std::string &login_, const std::string &passwd_) : login_(login_), passwd_(passwd_) ,root_(-1){ };
/** Argumentowy konstruktor użytkownika.
 * @param id_ id użytkownika
 * */
		User(long long int id_) : id_(id_) ,root_(-1) { }
/** Konstruktor bezargumentowy. */
		User(): root_(-1) { };
/**Metoda służąca do stworzenia obiektu string zawierającego informacje o podstawowych polach klasy. */
		std::string user_to_string();

		/*Getters and Setters*/
		void setRoot_(long long int root_);

		long long int getRoot_() const;

		void setPasswd_(const std::string &passwd_);

		const std::string &getPasswd_() const;

		void setLogin_(const std::string &login_);

		const std::string &getLogin_() const;

		void setId_(long long int id_);

		long long int getId_();


	private:
		long long id_; /**< Unikalne id użytkownika. */
		std::string login_; /**< Login użytkownika. */
		std::string passwd_; /**< Hasłow użytwkonika, zapisywane jako md5. */
		long long root_;/**< Id folderu głównego użtywkonika(Root dla wszystkich folderów tworzonych przez danego użytkownika). W nim znajdują się po stronie serwera wszystkie pliki.*/


	};
}
#endif //BACKUPER_USER_H
