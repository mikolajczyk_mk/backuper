/**
* @file user.h
* @brief Plik nagłówkowy klasy User.
* @author Marta Mikołajczyk
*/
#ifndef BACKUPER_CATALOG_H
#define BACKUPER_CATALOG_H
#define NOMINMAX
#include <string>
#include "soci.h"
#include <postgresql/soci-postgresql.h>
#include <log4cpp/Category.hh>

namespace common {
/**Klasa Catalog reprezentuje katalogi użytkowika. Zawiera potrzebne zmienne oraz funkcje do operacji na obiekcie catalog zarówno po stronie klienta jak i serwera.*/
	class Catalog {
	public:
/** Deklaracja przyjaźni potrzebna do operacji na bazie danych.*/
		friend class soci::type_conversion<Catalog>;
/** Argumentowy konstruktor katalogu.
 * @param name_ nazwa katalogu
 * @param parent_ id katalogu w którym znajduje się katalog
 * @param owner_ id właściciela katalogu
 * */
		Catalog(const std::string &name_, long long int parent_, long long int owner_) : name_(name_), parent_(parent_),
																						 owner_(owner_) { };

/** Argumentowy konstruktor katalogu.
 * @param id_ id katalogu
 * */
		Catalog(long long int id_) : id_(id_) { };
/** Konstruktor bezargumentowy. */
		Catalog() { };
/**Metoda służąca do stworzenia obiektu string zawierającego informacje o podstawowych polach klasy. */
		std::string catalog_to_string();

/*Getters and Setters*/
		long long int getId_() const;

		void setId_(long long int id_);

		const std::string &getName_() const;

		void setName_(const std::string &name_);


		long long int getOwner_() const;

		void setOwner_(long long int owner_);

		void setParent_(long long int parent_);

		long long int getParent_() const;


	private:
		long long id_; /**< Unikalne id katalogu*/
		std::string name_; /**< Nazwa katalogu wyświetlana użytkownikowi*/
		long long parent_;/**<  Id katalog w znaduje się dany katalog*/
		long long owner_;/**<  Id użytkownika do którego należey katalog*/

		log4cpp::Category &logger_ = log4cpp::Category::getInstance(std::string("server.common"));/**<  Zmienna pomocnicza służąca do logowania zdarzeń*/


	};
}
#endif //BACKUPER_CATALOG_H
