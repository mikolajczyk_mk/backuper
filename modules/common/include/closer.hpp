/**
* @file closer.hpp
* @brief Plik nag��wkowy klasy Closer.
* @author Krzysztof Kachniarz
*/

#ifndef BACKUPER_CLOSER_HPP
#define BACKUPER_CLOSER_HPP
/**Klasa s�u�aca jedynie do zamykania gniazdek.*/
class Closer {
public:
	/**Metoda wywo�uj�ca odpowiedni� funkcj� dla danego systemu zamykaj�c� gniazdko.
	* @param socket Gniazdko przeznaczone do zamkni�cia.
	*/
    static void closeSocket(int socket);
};

#endif //BACKUPER_CLOSER_HPP


