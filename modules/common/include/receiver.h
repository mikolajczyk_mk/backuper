/**
* @file receiver.h
* @brief Plik nag��wkowy klasy Receiver obs�uguj�cej w�a�ciwe pobieranie plik�w i liczb.
* @author Krzysztof Kachniarz
*/
#ifndef RECEIVER_H
#define RECEIVER_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>

/**Klasa odpowiedzialna za odbieranie plik�w.*/
class Receiver {
public:
	/**Konstruktor klasy Receiver.
	* @param file_handle Wska�nik do obiektu klasy FILE wskazuj�cej na miejce, w kt�rym nale�y zapisa� pobrany plik.
	* @param socket Gniazdko, przez kt�re b�dzie si� odbywa� transfer danych.
	*/
	Receiver(FILE *file_handle, int *socket) : file_handle_(file_handle), socket_(socket){};
	/**Konstruktor klasy Receiver.
	* @param socket Gniazdko, przez kt�re b�dzie si� odbywa� transfer danych.
	*/
	Receiver(int *socket) : socket_(socket){};
	/**Metoda inicjuj�ca transfer pliku.
	* @return Informacja, czy przesy�anie pliku przebieg�o pomy�lnie.
	*/
	bool receiveFile();
	
	bool readLongLong(long long *value);
private:
	/**Metoda obs�uguj�ca pobieranie pliku.
	* @return Informacja, czy plik zosta� pobrany prawid�owo.
	*/
	bool readFile();
	/**Metoda pobieraj�ca okre�lony bufor danych przez gniazdko.
	* @param buf Bufor przeznaczony do pobrania.
	* @param buf_len D�ugo�� bufora.
	*/
	bool readData(void *buf, int buflen);
	/**Metoda pobieraj�ca w odpowiedni spos�b warto�� typu long - rozmiar pliku.
	* @param value Warto��, kt�ra b�dzie przes�ana.
	* @return Informacja, czy pobieranie zosta�o przeprowadzone bez b��d�w. 
	*/
	bool readLong(unsigned long *value);
	int *socket_;
	FILE *file_handle_;
};

#endif //RECEIVER_H