//
// Created by ruda on 29.05.16.
//

#include "connection_provider.h"
#include <postgresql/soci-postgresql.h>
#include <iostream>
#include <soci.h>

namespace server { namespace db {
		ConnectionProvider &ConnectionProvider::getInstance() {
			static ConnectionProvider instance;
			return instance;
		}

		ConnectionProvider::ConnectionProvider():connectionPool_(common::Config::getPoolSize_()){
			boost::lock_guard<boost::mutex> guard(mutex_);
			int limit = common::Config::getPoolSize_();
			std::string db_path = common::Config::getDbPath_();
			logger_.debug("[ConnectionProvider] Pool Size is: %d.",limit);
			logger_.debug("[ConnectionProvider] Parameters to db are: %s",db_path.c_str());

			for(size_t i=0; i != limit; ++i){
				soci::session & sql = connectionPool_.at(i);
				sql.open(*soci::factory_postgresql(), db_path);
			}

		};

		PSession ConnectionProvider::getConncetion(){
			return std::unique_ptr<soci::session>{new soci::session(connectionPool_)};
		};


	}}