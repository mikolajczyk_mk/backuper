/**
* @file connection_provider.h
* @brief Plik nagłówkowy klasy ConnectionProvider.
* @author Marta Mikołajczyk
*/
#ifndef BACKUPER_CONNECTION_PROVIDER_H
#define BACKUPER_CONNECTION_PROVIDER_H


#include <soci.h>
#include <postgresql/soci-postgresql.h>
#include <boost/thread.hpp>
#include <log4cpp/Category.hh>

#include <config.h>

namespace server {
	namespace db {
/** Klasa odpowiedzialna za rozdzielanie połączeń do bazy danych. Realizowana za pomocą wzoraca singletona, ponieważ bibilioteka soci nie jest thread-safe. */
		typedef std::unique_ptr<soci::session> PSession;
		class ConnectionProvider {
		public:
/** Metoda zwracająca referecje do ConnectionProvidera*/
			static ConnectionProvider &getInstance();
/** Metoda zwracajaca unique_ptr do sesji do bazy danych
 * @retrun unique_ptr do sesji do bazy dancyh*/
			PSession getConncetion();
		private:
			boost::mutex mutex_; /**<  Zmienna pomocnicza służąca zakładania sekcji krytycznej podczas inicjowania sesji*/
			soci::connection_pool connectionPool_; /**<  Przechowuje informacje o puli połączeń*/
/** Prywatny konstruktro bezargumentowy*/
			ConnectionProvider();

/** Zbaronione konstruktor kopiujący*/
			ConnectionProvider (const ConnectionProvider &) = delete;
/** Zarboniony operator przypisania*/
			ConnectionProvider &operator=(const ConnectionProvider &) = delete;
			log4cpp::Category &logger_ = log4cpp::Category::getInstance(std::string("server.db")); /**<  Zmienna pomocnicza służąca do logowania zdarzeń*/

		};
	}}

#endif //BACKUPER_CONNECTION_PROVIDER_H
