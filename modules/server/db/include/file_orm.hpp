//
// Created by ruda on 16.05.16.
//

#ifndef BACKUPER_FILEDB_H
#define BACKUPER_FILEDB_H

#include "soci.h"
#include "file.h"
#include "catalog.h"
#include "user.h"


namespace soci {
/** ORM dla pliku*/
	template <> struct type_conversion<common::File>{

		typedef values base_type;
		static common::File from_base(base_type v,indicator ind, common::File &file){
			file.id_=v.get<long long>("id");
			file.name_=v.get<std::string>("name");
			file.filesystemName_=v.get<std::string>("in_filesystem_name");
			file.parent_=v.get<long long>("parent");
			file.version_=v.get<long long>("version",-1);
			file.owner_=v.get<long long>("owner");

			return file;
		}
		static void to_base (const common::File &file, base_type & v, indicator & ind){
			v.set("id",file.id_);
			v.set("name",file.name_);
			v.set("in_filesystem_name",file.filesystemName_);
			v.set("parent",file.parent_);
			v.set("version", file.version_, file.version_== -1 ? i_null : i_ok);
			v.set("owner",file.owner_);

		}
	};
/** ORM dla katalogu*/
	template <> struct type_conversion<common::Catalog>{
		typedef values base_type;
		static common::Catalog from_base(base_type v,indicator ind, common::Catalog &catalog){

			catalog.id_=v.get<long long>("id");
			catalog.name_=v.get<std::string>("name");
			catalog.parent_=v.get<long long>("parent");
			catalog.owner_=v.get<long long>("owner");

			return catalog;
		}
		static void to_base (const common::Catalog &catalog, base_type & v, indicator & ind){

			v.set("id",catalog.id_);
			v.set("name",catalog.name_);
			v.set("parent",catalog.parent_);
			v.set("owner",catalog.owner_);

		}
	};
/** ORM dla użytkownika*/
	template <> struct type_conversion<common::User>{
		typedef values base_type;

		static common::User from_base(base_type v,indicator ind, common::User &user){

			user.id_=v.get<long long>("id");
			user.login_=v.get<std::string>("login");
			user.passwd_=v.get<std::string>("passwd");
			user.root_=v.get<long long>("root",-1);

			return user;
		}
		static void to_base (const common::User &user, values & v, indicator & ind){

			v.set("id", user.id_);
			v.set("login",user.login_);
			v.set("passwd",user.passwd_);
			v.set("root",user.root_, user.root_ == -1 ? i_null : i_ok );
			ind = i_ok;
		}
	};

}
#endif //BACKUPER_FILEDB_H
