#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN
#endif

#pragma once
#include "handler.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <thread>
#include <string>
#include <time.h>
#include "protocol.h"
#include "receiver.h"
#include "sender.h"
#include <iomanip>
#include <ctime>
#include <chrono>
#include <sstream>
#include <string>
#include <vector>
#include "closer.hpp"
#include <user_service.h>
#include "user.h"
#include "file_service.h"
#include "catalog_service.h"
#include <boost/filesystem.hpp>
#include "exception/clientdecision_exception.h"
#include "exception/registration_exception.h"
#include "exception/validateuser_exception.h"

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#pragma comment(lib, "Ws2_32.lib")
#elif __linux__

#include <zconf.h>
#include <exception/validateuser_exception.h>

#endif


#define DEFAULT_BUFLEN 512

int Handler::handleConnection(int *clientSocket){
	//waiting for requests
	socket_ = clientSocket;
	while (true){
		char recv_buf[DEFAULT_BUFLEN];

		int result = recv(*clientSocket, recv_buf, DEFAULT_BUFLEN, 0);
		if (result <= 0) break;

		switch (*recv_buf){
		case Protocol::LOGIN:
			logIn(true);
			break;
		case Protocol::REGISTER:
			logIn(false);
			break;
		case Protocol::UPLOAD_FILE:
			receiveFile();
			break;
		case Protocol::DOWNLOAD_FILE:
			sendFile();
			break;
		case Protocol::SHOW_FILES:
			sendFileList();
			break;
		case Protocol::UPLOAD_DIRECTORY:
			receiveDirectory();
			break;
		case Protocol::SHOW_DIRECTORIES:
			sendCatalogList();
			break;
		case Protocol::DOWNLOAD_CATALOG:
			sendCatalog();
			break;
		case Protocol::REMOVE_CATALOG:
			removeCatalog();
			break;
		case Protocol::REMOVE_FILE:
			removeFile();
			break;
		}
	}
	Closer::closeSocket(*socket_);
	return 0;
}

void Handler::logIn(bool account) {
	char recv_buf[DEFAULT_BUFLEN];
	char tmp = Protocol::SEND_LOGIN;
	std::string login;
	std::string password;
	send(*socket_, &tmp, DEFAULT_BUFLEN, 0);
	recv(*socket_, recv_buf, DEFAULT_BUFLEN, 0);
	login = recv_buf;
	tmp = Protocol::SEND_PASSWORD;
	send(*socket_, &tmp, DEFAULT_BUFLEN, 0);
	recv(*socket_, recv_buf, DEFAULT_BUFLEN, 0);
	password = recv_buf;
	server::service::UserService user_service;
	logger_.debug("Login " + login);
	if (!account) {
		authorized_ = false;
		try {
			user_ = user_service.registrationUser(login, password);
			authorized_ = true;
		} catch (common::exception::RegistrationException &re) {
			logger_.info("[Handler, logIn] Registatrion;  %s", re.what());
			authorized_ = false;
		} catch (...) {
			logger_.info("[Handler, logIn] Registatrion; Something goes wrong...");
			authorized_ = false;
		}
	}
	else {
		try {
			user_ = user_service.validateUser(login, password);
			authorized_ = true;
		}
		catch (common::exception::ValidateUserException &vue) {
			authorized_ = false;
			logger_.info("[Handler, logIn] Login; %s", vue.what());
		}
		catch (...) {
			authorized_ = false;
			logger_.info("[Handler, logIn] Login; Something goes wrong...");
		}
	}

		if (authorized_)
			tmp = Protocol::ACK;
		else
			tmp = Protocol::NACK;

		send(*socket_, &tmp, DEFAULT_BUFLEN, 0);
}

char *Handler::getFilenameFromPath(char *path) {
	char *token, *filename;
	filename = token = strtok(path, "/");
	for (; (token = strtok(NULL, "/")) != NULL; filename = token);
	std::cout << "filename : " << filename << std::endl;
	return filename;
}


void Handler::receiveFile(){
	char recv_buf[DEFAULT_BUFLEN];
	char send_file_name = Protocol::SEND_FILE_NAME;
	char ack = Protocol::ACK;
	char nack = Protocol::NACK;
	char chksm = Protocol::SEND_CHECK_SUM;

	send(*socket_, &send_file_name, DEFAULT_BUFLEN, 0); //SEND_FILE_NAME
	recv(*socket_, recv_buf, DEFAULT_BUFLEN, 0); //receive file name

	char* filename = getFilenameFromPath(recv_buf);
	
	//recv catalog id
	Receiver receiver_long = Receiver(socket_);
	long long id_folder;
	long long size;
	receiver_long.readLongLong(&id_folder);
	receiver_long.readLongLong(&size);
	if (id_folder == 0)
		id_folder = user_.getRoot_();
	bool file_exist;
	ClientDecision decision;

	server::service::FileService file_service;
	if (!file_service.isSuchFile(filename, id_folder, user_.getId_())){
		std::cout << "file doesnt exist " << std::endl;
		file_exist = false;
		decision = ClientDecision::ADD;
	}
	else{
		std::cout << "file exists " << std::endl;
		file_exist = true;
	}

	if (!file_exist){
		common::File file = common::File(filename, id_folder, user_.getId_());
		file.setSize_(size);
		long long file_id = file_service.addFile(decision, file);
		std::string path = common::Config::getLocationMasterRoot_() +
							 std::to_string(user_.getId_()) +
							common::Config::getPathSeparator_() +
							std::to_string(file_id);
		std::cout << " path: " << path << std::endl;
		send(*socket_, &ack, DEFAULT_BUFLEN, 0); //ACK
		FILE *file_handler = fopen(path.c_str(), "wb");
		Receiver receiver = Receiver(file_handler, socket_);
		receiver.receiveFile();
	}
	else{
		send(*socket_, &nack, DEFAULT_BUFLEN, 0); //ACK
		std::cout << "File exists" << std::endl;
	}

}

void Handler::receiveDirectory(){
	char recv_buf[DEFAULT_BUFLEN];
	char tmp;
	long long id_folder_parent;
	long long id_folder;
	std::string name_catalog;
	server::service::CatalogService catalog_service;
	Receiver receiver = Receiver(socket_);
	Sender sender = Sender(socket_);
	recv(*socket_, recv_buf, DEFAULT_BUFLEN, 0); //dir name
	name_catalog = getFilenameFromPath(recv_buf);
	receiver.readLongLong(&id_folder_parent); //parent id
	if (id_folder_parent == 0)
		id_folder_parent = user_.getRoot_();
	if (!catalog_service.isSuchCatalog(name_catalog, id_folder_parent, user_.getId_())){
		id_folder = catalog_service.addCatalog(ClientDecision::ADD, name_catalog, id_folder_parent, user_.getId_());
		tmp = Protocol::ACK;
		send(*socket_, &tmp, DEFAULT_BUFLEN, 0); //ACK
		sender.sendLongLong(id_folder);
	}
}

void Handler::sendFile(){
	char ack_buf = Protocol::ACK;
	long long id;
	Receiver receiver = Receiver(socket_);
	receiver.readLongLong(&id); //ID
	common::File file(id);
	server::service::FileService file_service;
	file_service.completeFileInformation(file);
	std::string path = common::Config::getLocationMasterRoot_() +
		std::to_string(user_.getId_()) +
		common::Config::getPathSeparator_() +
		std::to_string(file.getId_());
	std::cout << " path: " << path << std::endl;
	send(*socket_, &ack_buf, DEFAULT_BUFLEN, 0); //ACK
	FILE *file_handler = fopen(path.c_str(), "rb");
	Sender sender = Sender(file_handler, socket_);
	sender.sendFile();
}

void Handler::sendCatalog(){
	long long parent_id_catalog;
	Receiver receiver(socket_);
	Sender sender(socket_);
	receiver.readLongLong(&parent_id_catalog);
	if (parent_id_catalog == 0)
		parent_id_catalog = user_.getRoot_();
	server::service::CatalogService catalog_service;
	server::service::FileService file_service;

	std::vector<common::Catalog> catalogs;
	catalog_service.getListOfCatalogs(parent_id_catalog, catalogs);

	std::vector<common::File> files;
	file_service.getLastVersionFilesFromCatalog(parent_id_catalog, files);

	sender.sendLongLong(catalogs.size());

	for (common::Catalog catalog : catalogs){
		catalog_service.completeCatalogInformation(catalog);
		std::string name = catalog.getName_();
		long long id_catalog = catalog.getId_();
		send(*socket_, name.c_str(), DEFAULT_BUFLEN, 0);
		sender.sendLongLong(id_catalog);
		sendCatalog();
	}

	sender.sendLongLong(files.size());

	for (common::File file : files){
		file_service.completeFileInformation(file);
		std::string name = file.getName_();
		send(*socket_, name.c_str(), DEFAULT_BUFLEN, 0);
		sender.sendLongLong(file.getId_());
		sendFile();
	}
}

void Handler::sendFileList(){
	char ack = Protocol::ACK;

	Receiver receiver_long = Receiver(socket_);
	long long int id_folder;
	receiver_long.readLongLong(&id_folder);
	if (id_folder == 0)
		id_folder = user_.getRoot_();

	std::vector<common::File> files;
	server::service::FileService file_service;
	file_service.getLastVersionFilesFromCatalog(id_folder, files);

	Sender sender_long = Sender(socket_);
	sender_long.sendLongLong(files.size());

	for (common::File file : files) {
		sendFileInfo(file);
	}
}

void Handler::sendFileInfo(common::File file) {
	server::service::FileService file_service;
	file_service.addInformationSizeAndLastTimeWrite(file);
	file_service.addInformationOfNumberOfVersion(file);
	std::string name = file.getName_();
	long long size = file.getSize_();
	long long number_of_versions = file.getNumberOfVersion_();
	long long id = file.getId_();
	Sender sender = Sender(socket_);
	send(*socket_, name.c_str(), DEFAULT_BUFLEN, 0);
	sender.sendLongLong(size);
	sender.sendLongLong(number_of_versions);
	sender.sendLongLong(id);
}


void Handler::sendCatalogList(){
	Receiver receiver_long = Receiver(socket_);
	long long int id_folder;
	receiver_long.readLongLong(&id_folder);
	if (id_folder == 0)
		id_folder = user_.getRoot_();
	std::vector<common::Catalog> catalogs;
	server::service::CatalogService catalog_service;
	catalog_service.getListOfCatalogs(id_folder, catalogs);
	Sender sender_long = Sender(socket_);
	sender_long.sendLongLong(catalogs.size()); //send num of catalogs
	for (common::Catalog catalog : catalogs){
		sendCatalogInfo(catalog);
	}
}

void Handler::sendCatalogInfo(common::Catalog catalog){
	server::service::CatalogService catalog_service;
	catalog_service.completeCatalogInformation(catalog);
	std::string name = catalog.getName_();
	long long catalog_id = catalog.getId_();
	send(*socket_, name.c_str(), DEFAULT_BUFLEN, 0);
	Sender sender(socket_);
	sender.sendLongLong(catalog_id);
}

void Handler::removeCatalog(){
	Receiver receiver(socket_);
	long long id_catalog;
	receiver.readLongLong(&id_catalog);
	if (id_catalog == 0)
		id_catalog = user_.getRoot_();
	removeCatalogFromFilesystem(id_catalog);
	server::service::FileService file_service;
	file_service.removeAllFileFromCatalog(id_catalog, true);
}

void Handler::removeFileFromFilesystem(long long id_file){
	server::service::FileService file_service;
	std::string name = common::Config::getLocationMasterRoot_() +
		std::to_string(user_.getId_()) +
		common::Config::getPathSeparator_() +
		std::to_string(id_file);
	
	boost::filesystem::path path_file(name);
	if (exists(path_file))
		boost::filesystem::remove(path_file);
}

void Handler::removeCatalogFromFilesystem(long long id_catalog){
	server::service::CatalogService catalog_service;
	server::service::FileService file_service;
	std::vector<common::Catalog> catalogs;
	std::vector<common::File> files;
	catalog_service.getListOfCatalogs(id_catalog, catalogs);

	for (common::Catalog catalog : catalogs){
		removeCatalogFromFilesystem(catalog.getId_());
	}
	file_service.removeAllFileFromCatalog(id_catalog, true);
}

void Handler::removeFile(){
	char recv_buf[DEFAULT_BUFLEN];
	Receiver receiver(socket_);
	long long id_file;
	receiver.readLongLong(&id_file);
	server::service::FileService file_service;
	file_service.removeFile(ClientDecision::ALL, id_file);
}