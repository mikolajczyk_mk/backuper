//
// Created by ruda on 15.04.16.
//

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <iostream>
#include <cmath>
#include "controller.h"
#include "handler.h"
#include "closer.hpp"
#include "md5.h"
#include <string>
#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
	#include <windows.h>
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#pragma comment(lib, "Ws2_32.lib")
#elif __linux__
	#include <zconf.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <netdb.h>  /* Needed for getaddrinfo() and freeaddrinfo() */
	#include <unistd.h> /* Needed for close() */
#endif

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT 27015

void Controller::startHandler(int *socket){
	Handler handler;
	handler.handleConnection(socket);
}

int Controller::listenForConnections(void) {
	//initialize winsock
	#ifdef _WIN32
		WSADATA wsa_data;
		WSAStartup(MAKEWORD(2, 2), &wsa_data);
	#endif
	int* client_socket = 0;
	int this_socket;
	struct sockaddr_in destination;

	destination.sin_family = AF_INET;
	this_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this_socket < 0) {
		std::cout<<"Socket Creation FAILED!" << std::endl;
		return 0;
	}

	destination.sin_port = htons(DEFAULT_PORT);
	destination.sin_addr.s_addr = INADDR_ANY;
	if (bind(this_socket, (struct sockaddr *) &destination, sizeof(destination)) < 0) {
		std::cout << "Binding Socket FAILED!" << std::endl;
		if (socket) Closer::closeSocket(this_socket);
		return 0;
	}

	std::cout << "Listening on " << DEFAULT_PORT << std::endl;
	if (listen(this_socket, 5) < 0) {
		std::cout << "Listening on Socket FAILED!" << std::endl;
		if (socket) Closer::closeSocket(this_socket);
		return 0;
	}

	struct sockaddr_in client_address;
	int client_size = sizeof(client_address);

	int temp_socket = 0;
	//waiting for connections
	while (true) {
	#ifdef _WIN32
		temp_socket = accept(this_socket, (struct sockaddr *) &client_address, (int *)&client_size);
	#elif __linux__
		temp_socket =  accept(this_socket, (struct sockaddr *) &clientAddress, (unsigned int *)&clientSize);
	#endif
		client_socket = &temp_socket;
		if (client_socket < 0) {
			std::cout << "Socket Connection FAILED!" << std::endl;
			if (client_socket) Closer::closeSocket(*client_socket);
		}
		std::cout << "Connection Established!" << std::endl;

		threads_.push_back(std::thread(&Controller::startHandler, this, client_socket));
	}

	Closer::closeSocket(this_socket);

	return 0;

}