/**
* @brief Plik nag��wkowy klasy Controller.
* @author Krzysztof Kachniarz
*/

#ifndef BUCKUPER_CONTROLLER_H
#define BUCKUPER_CONTROLLER_H

#include <cmath>
#include <vector>
#include <thread>

/**Klasa Controller realizuj�ca logik� biznesow� zwi�zan� z nawi�zywaniem po��cze� serwera z klientem*/
class Controller {
public:
	/**Metoda uruchamiaj�ca nas�uchiwanie po��cze�.*/
	int listenForConnections(void);
	/**Metoda uruchamiaj�ca obs�ug� danego po��czenia przez klas� Handler.
	* @param socket Gniazdko, kt�re ma by� przekazane do obiektu klasy Handler.
	*/
	void startHandler(int *socket);
private:
	std::vector<std::thread> threads_;
};

#endif //BUCKUPER_CONTROLLER_H