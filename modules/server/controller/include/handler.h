#include <stdio.h>
#include "user.h"
#include <file.h>
#include "catalog.h"

#include "log4cpp/Category.hh"
#include <log4cpp/PropertyConfigurator.hh>
#include "log4cpp/Appender.hh"
#include "log4cpp/FileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/BasicLayout.hh"
#include "log4cpp/Priority.hh"
#include "log4cpp/Priority.hh"
#include <exception>
#include "exception/registration_exception.h"

#ifdef _WIN32
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <windows.h>
	#pragma comment(lib, "Ws2_32.lib")
#endif

#ifdef __linux__
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
#include <zconf.h>


////void closesocket(int socket) { close(socket); }
	#include <zconf.h>
#endif

/**Klasa s�u��ca do obs�ugi ��da� od klienta.*/
class Handler {

public: 
	/**Metoda g��wna obs�uguj�ca przychodz�ce ��dania z wcze�niej ustanowionego po��czenia.
	* @param client_socket Gniazdko na kt�rym zosta�o ustanowione po��czenie.
	*/
	int handleConnection(int *client_socket);
private:
	/**Metoda obs�uguj�ca wymian� danych potrzebnych do zalogowania u�ytkownika.
	* @param account Informacja, czy u�ytkownik kt�ry chce si� zalogowa� ma konto czy nie. W zale�no�ci od tego parametru zak�adane jest konto b�d� nie.
	*/
	void logIn(bool account);
	/**Metoda obs�uguj�ca wymian� danych potrzebnych do przes�ania pliku do klienta.*/
	void sendFile();
	/**Metoda obs�uguj�ca wymian� danych potrzebnych do przes�ania folderu do klienta.*/
	void sendCatalog();
	/**Metoda wysy�aj�ca dane na temat zgromadzonych plik�w do klienta.*/
	void sendFileList();
	/**Metoda wysy�aj�ca dane na temat zgromadzonych katalog�w do klienta.*/
	void sendCatalogList();
	/**Metoda wysy�aj�ca szczeg�y na temat danego pliku do klienta*/
	void sendFileInfo(common::File file);
	/**Metoda wysy�aj�ca do klienta szczeg�y na temat danego folderu.*/
	void sendCatalogInfo(common::Catalog);
	/**Metoda obs�uguj�ca wymian� danych potrzebnych do usuni�cia katalogu.*/
	void removeCatalog();
	/**Metoda usuwaj�ca pliki z dysku i bazy danych.
	* @param id_file Identyfikator folderu z kt�rego maj� zosta� usuni�te pliki.
	*/
	void removeCatalogFromFilesystem(long long id_file);
	/**Metoda obs�uguj�ca wymian� danych potrzebnych do usuni�cia pliku.*/
	void removeFile();
	/**Metoda usuwaj�ca plik z dysku oraz bazy danych.
	* @param id_file Identyfikator pliku, kt�ry ma zosta� usuni�ty.
	*/
	void removeFileFromFilesystem(long long id_file);
	/**Metoda wycinaj�ca nazw� pliku lub ostatniego katalogu z przes�anej przez klienta �cie�ki.
	* @param path �cie�ka z kt�rej ma by� wyci�ta nazwa pliku b�d� ostatniego folderu.
	*/
	char * getFilenameFromPath(char * path);
	/**Metoda obs�uguj�ca wymian� danych potrzebnych do odebrania pliku od klienta.*/
	void receiveFile();
	/**Metoda obs�uguj�ca wymian� danych potrzebnych do odebrania katalogu od klienta*/
	void receiveDirectory();
	int* socket_;
	common::User user_;
	bool authorized_;
	log4cpp::Category &logger_ = log4cpp::Category::getInstance(std::string("server.controller"));
};