/**
* @file file_service.h
* @brief Plik nagłówkowy klasy FileService.
* @author Marta Mikołajczyk
*/

#ifndef BACKUPER_FILE_SERVICE_H
#define BACKUPER_FILE_SERVICE_H

#include <string>
#include <iostream>
#include <file.h>
#include <connection_provider.h>
#include <soci.h>
#include <postgresql/soci-postgresql.h>
#include <vector>
#include <client_decision.h>
#include <utility>
#include <algorithm>
#include "catalog.h"
#include "user.h"

namespace server {
	namespace service {
/**Klasa FileService realizująca logikę biznesową związaną z plikami.*/
		class FileService {
			typedef std::pair<ClientDecision, common::File> actionOnFile;

		public:
/** Metoda sprawdzająca czy istnieją inne wersje danego pliku niż on sam
 * @param file plik którego inne wesje są szukane
 * @return czy są inne werjse*/
			bool areVersionsOfFile(common::File file);
/**Metoda dodająca nowy plik do bazy danych w zależności od tego co należy z nim zrobić (Add, VERSION,REPlACE z @ref common::Config). Wywołuje metodę: @ref addFile(ClientDecision client_decision, common::File file)
 * @param client_decision dyrektywa co należy zrobić z plikiem,
 * @param file_name nazwa pliku
 * @param id_catalog katalog do którego jest dodawany plik
 * @param id_user właściciel pliku.
 * retrun id które otrzymał plik*/
			long long int addFile(ClientDecision client_decision, std::string file_name, long long int id_catalog, long long int id_user);
/** Metoda dodająca nowy plik do bazy danych w zależności od podanej dyrektywy. Następuje oddlegowanie odpowiednich pod funkcji w zależności od client_decision.
 * @client_decision dyrektywa co należy zrobić z plikiem. @ref common::Config
 * @file plik uzupełniony o potrzebny kontekst name_,parent_, owner_
 * retrun id które otrzymał plik*/
			long long int addFile(ClientDecision client_decision, common::File file);
/** Metoda dodająca nowy plik do bazy danych w zależności od podanej dyrektywy. Wywołuje: @ref addFile(ClientDecision client_decision, common::File file);
 * @pair zawiera dyrektywa co należy zrobić z plikiem. @ref common::Config oraz jego samego uzupełnionego o potrzebny kontekst name_,parent_, owner_
 * retrun id które otrzymał plik*/
			long long int addFile(std::pair<ClientDecision ,common::File> pair);
/** Metoda dodająca nowe pliki do bazy danych w zależności od podanej dla niego dyrektywy. Wywołuje: @ref addFile(ClientDecision client_decision, common::File file);
 * @v zawiera wektor pari składających się każda z dyrektywy co należy zrobić z danycm z pary plikiem. @ref common::Config oraz jego nim uzupełnionym o potrzebny kontekst name_,parent_, owner_
 * @retrun id które otrzymał plik*/
			std::vector<long long int> addFiles(std::vector<actionOnFile> v);

/**Metoda dodająca nową wersję pliku. Wywołuje addNewVersion(common::File new_version, common::File old_version)
 * @param file nowa wersja pliku.
 * @param id_old_version id ostatniej wersji
 * @parame setOldName flaga mówiąca czy zachować starą nazwę czy dać nową
 * retrun id które otrzymał plik*/
			long long int addNewVersion(common::File file, long long int id_old_version, bool setOldName);
/**Metoda dodająca nową werjsę pliku.
 * @param new_version nowa wersja pliku
 * @param old_version ostatnia wersja pliku
 * @retrun id które otrzymał plik*/
			long long int addNewVersion(common::File new_version, common::File old_version);
/**Metoda dodająca nową werjsę pliku. addNewVersion(common::File file, long long int id_old_version, bool setOldName);
 * @param new_version nowa wersja pliku
 * @param old_version ostatnia wersja pliku
 * @retrun id które otrzymał plik*/
			long long int addNewVersionWithNewName(std::string new_name, long long int id_old_version);
/**Metoda dodająca nową werjsę pliku. Wywołuje addNewVersion(common::File new_version, common::File old_version)
 * @param new_version nowa wersja pliku
 * @param old_version ostatnia wersja pliku
 * @retrun id które otrzymał plik*/
			long long int addNewVersionWithoutChangeName(long long int id_old_version);
/** Metoda uzupełniajaca plik o dodatkowe informacje: rozmiar oraz datę ostaniej modyfikacji. Plik musi posiadać wypełnione id
 * @param file plik w którym uzupełniane informacje*/
			void addInformationSizeAndLastTimeWrite(common::File &file);
/** Metoda uzupełniajaca plik o dodatkowe informacje: ilość wersji. Plik musi posiadać wypełnione id
 * @param file plik w którym uzupełniane informacje*/
			void addInformationOfNumberOfVersion(common::File &file);
/** Metoda uzupełniająca wszystkie podstawowe informacje o pliku na postawie jego id.
 * @param file plik który jest uzupełniany*/
			void completeFileInformation(common::File &file);
/** Metoda uzupełniająca dany vektor plików o wszystkie pliki, wraz z wersjami,znajdujące się we wskazanym katalogu.
 * @param id_catalog katalog w którym szukamy
 * @param wektor plików znajdujący się w podanym katalogu
 * */
			void getAllFilesAndVersion(long long int id_catalog, std::vector<common::File> &v);
/** Metoda uzupełniająca dany vektor plików o wszystkie pliki, wraz z wersjami, znajdujące się we wskazanym katalogu.
 * @param catalog katalog w którym szukamy
 * @param wektor plików znajdujący się w podanym katalogu
 * */
			void getAllFilesAndVersion(common::Catalog catalog, std::vector<common::File> &v);

/** Metoda uzupełniająca dany vektor plików o wszystkie jego wersje. Sprawdzając nie ostatnią wersje pliku zwróci wszykie wersje, łącznie z tymi dla których on sam jest przodkiem.
 * @param id_file id pliku którego wersji szukamy
 * @param wektor plików znajdujący się w podanym katalogu
 * */
			void getAllVersionOfFile(long long int id_file, std::vector<common::File> &files);
/** Metoda uzupełniająca dany vektor plików o wszystkie jego wersje. Sprawdzając nie ostatnią wersje pliku zwróci wszykie wersje, łącznie z tymi dla których on sam jest przodkiem.
 * @param file  pliku którego wersji szukamy, koniecznie musi mieć uzupełnione version
 * @param wektor plików znajdujący się w podanym katalogu
 * */
			void getAllVersionOfFile(common::File file, std::vector<common::File> &files);
/** Metoda uzupełniająca dany vektor plików o wszystkie pliki(tylko ostatnie wersje) znajdujące się we wskazanym katalogu.
 * @param id_catalog katalog w którym szukamy
 * @param wektor plików znajdujący się w podanym katalogu
 * */
			void getLastVersionFilesFromCatalog(long long int id_catalog, std::vector<common::File> &files);
/** Metoda uzupełniająca dany vektor plików o wszystkie pliki(tylko ostatnie wersje) wraz z wersjami znajdujące się we wskazanym katalogu.
 * @param catalog katalog w którym szukamy
 * @param wektor plików znajdujący się w podanym katalogu
 * */
			void getLastVersionFromCatalog(common::Catalog catalog, std::vector<common::File> &files);
/** Metoda zwracająca id przedostatniej wersji pliku. Plik musi mieć uzupełnione id
 * @param file plik w którym uzupełniane informacje*/
			long long int givePenultimateVersion(common::File &file);

/** Metoda sprawdzająca czy użytkownik ma plik o takiej nazwie w danym katalogu
 * @param name nazwa pliku
 * @param parent id katalogu w którym znajduje się szukany plik
 * @param id_user id użytkownika dla którego wykonywana jest akcja
 * @return true- isteniej plik o takiej nazwie, w takiej lokalizacji dla tego użytkownika
 * */
			bool isSuchFile(std::string file_name, long long int id_catalog, long long int id_user);


			/*Remove Function*/
/** Metoda usuwająca wszystkie pliki z danego katalogu. Wywołuje @ref removeAllFileFromCatalog(common::Catalog catalog,bool delete_with_catalog=false);
 * @param id_catalog id katalogu z którego mają zostać usuniete pliki (z bazy danych i ich fizyczna reprezentacja)
 * delete_with_catalog flaga mówiąca czy dany katalog ma również zostać usunięty*/
			void removeAllFileFromCatalog(long id_catalog, bool delete_with_catalog=false );
/** Metoda usuwająca wszystkie pliki z danego katalogu.
 * @param id_catalog id katalogu z którego mają zostać usuniete pliki (z bazy danych i ich fizyczna reprezentacja)
 * delete_with_catalog flaga mówiąca czy dany katalog ma również zostać usunięty*/
			void removeAllFileFromCatalog(common::Catalog catalog,bool delete_with_catalog=false);
/** Metoda usuwająca wszyskie instancje danej wersji pliku, łącznie z nią samą.
 * @param client_decision metoda jest używana w dwóch kontekstach różniących się danymi, ALL czyli usuwanie pliku, oraz
 * REPLACE przy dodawaniu nowego plku - zastępowanie daotychczas istniejącego pliku (i wszystkich jego wersji)
 * @param file usuwany plik
 * */
			void removeAllVersion(ClientDecision client_decision, common::File file);

/** Metoda usuwająca plik. Wowołuje @ref removeFile(ClientDecision client_decision, common::File file)
 * @param client_decision decyzja czy tylko to wystąpienie tego pliku czy wszystkie instancje
 * @param id id usuwanego plku*/
			void removeFile(ClientDecision client_decision, long long int id);
/** Metoda usuwająca plik. Wowołuje @ref removeFile(ClientDecision client_decision, common::File file)
 * @param client_decision decyzja czy tylko to wystąpienie tego pliku czy wszystkie instancje
 * @param id id usuwanego plku
 * @param user użytkownik w kontekście któego to się dzieje*/
			void removeFile(ClientDecision client_decision, long long int id, common::User user);
/** Metoda usuwająca plik. Wowołuje @ref removeFile(ClientDecision client_decision, common::File file)
 * @param client_decision decyzja czy tylko to wystąpienie tego pliku czy wszystkie instancje
 * @param id id usuwanego plku
 * @param id_user id użytkownika w kontekście któego to się dzieje*/
			void removeFile(ClientDecision client_decision, long long int id, long long int id_user);
/** Metoda usuwająca plik. Następuje oddlegowanie odpowiednich pod funkcji w zależności od client_decision.
 * @param client_decision decyzja czy tylko to wystąpienie tego pliku czy wszystkie instancje
 * @param id id usuwanego plku
 * @param id_user id użytkownika w kontekście któego to się dzieje*/
			void removeFile(ClientDecision client_decision, common::File file);
/** Metoda usuwająca plik. Wowołuje @ref removeFile(ClientDecision client_decision, common::File file)
 * @param pair dyrektywa co należy zrobić z plikiem oraz plik uzupłeniony o wymagane pola
 * */
			void removeFile(std::pair<ClientDecision ,common::File> pair);
/** Metoda usuwająca plik. Wowołuje @ref removeFile(ClientDecision client_decision, common::File file)
 * @param action_on_files wektor składajacy sie ze struktury pair, składającej się z  dyrektywy co należy zrobić z plikiem oraz plik uzupłeniony o wymagane pola
 * */
			void removeFile(std::vector<actionOnFile> action_on_files);

/** Metoda zwracajac id ostatniej wersji podanego pliku
 * @param new_file plik którego ostatnia wersja jest szukana
 * @return id ostatniej wersji podanego pliku*/
			long long int selectIdLastVersionFile(common::File new_file);

		private:
			log4cpp::Category &logger_ = log4cpp::Category::getInstance(std::string("server.service"));  /**<  Zmienna pomocnicza służąca do logowania zdarzeń*/
			long long int addNewFile(common::File new_file);
/** Metoda uzupełniajaca plik o id oraz wersję danego pliku. Pobierająca z bazy danych z sekwencji następne id. Do nowych plików.
 * @param new_file plik którego pola są uzupełniane
 * @return nowe id*/
			void setIdAndVersion(common::File &new_version, std::unique_ptr<soci::session> &sql) const;
/** Metoda usuwająca plik z bazy danych oraz jego fizyczną reprezentacje
 * @param file usuwany plik*/
			void removeSelectedFile(common::File file);



		};

	}
}
#endif //BACKUPER_FILE_SERVICE_H
