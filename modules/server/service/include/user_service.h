/**
* @file user_service.h
* @brief Plik nagłówkowy klasy UserService.
* @author Marta Mikołajczyk
*/

#ifndef BACKUPER_USER_SERVICE_H
#define BACKUPER_USER_SERVICE_H

#include <string>
#include <iostream>
#include <log4cpp/Category.hh>
#include "user.h"
#include "catalog.h"
#include "connection_provider.h"
#include <soci.h>
#include <postgresql/soci-postgresql.h>
#include "exception/registration_exception.h"
#include <exception>
namespace server {
	namespace service {
/**Klasa UserService realizująca logikę biznesową związaną z użytkownikiem.*/
		class UserService {
		public:

/** Metoda dodająca nowego użtywkonika na podstawie jego loginu i hasła. Wywołuje @ref addNewUser(common::User user)
 * @param login login
 * @param passwd hasło
 * @return użtywkonik uzupełniony o wszystkie podstawowe pola
 * */
			common::User addNewUser(std::string login, std::string passwd);
/** Metoda dodająca nowego użtywkonika.
 * @param user uzupełnione pola login i hasło
 * @return użtywkonik uzupełniony o wszystkie podstawowe pola
 * */
			common::User addNewUser(common::User user);
/** Metoda zmieniająca hasła gdy podane jest stare. Wywołuje @ref changePasswdWithOldPasswd(common::User user, std::string new_passwd);
 * @param login login
 * @param passwd stare hasło
 * @param new_passwd nowe hasło*/
			void changePasswdWithOldPasswd(std::string login, std::string passwd, std::string new_passwd);
/** Metoda zmieniająca hasła gdy podane jest stare.
 * @param user użytkownik
 * @param new_passwd nowe hasło*/
			void changePasswdWithOldPasswd(common::User user, std::string new_passwd);
/**Metoda zmieniająca hasła gdy nie podane jest stare.
 * @param user zwiera login oraz nowe hasło*/
			void changePasswdWithoutOldPasswd(common::User user);
/**Metoda sprawdzająca czy dany login jest w bazie danych
 * @param login login
 * @retrun czy jest w bazie danych*/
			bool isLoginInDatabase(const std::string &login);

/** Metoda rejestrująca użtywkonika, jeżeli podany login jest już wykorzystywany rzucany jest wyjątek: @ref common::exception::RegistrationException(). Wywołuje: registrationUser(common::User user)
 * @param login login
 * @param passwd hasło
 * @retrun użytkownik o uzuepłnionych wszyskich podstawowych polach*/
			common::User registrationUser(std::string login, std::string passwd);
/** Metoda rejestrująca użtywkonika, jeżeli podany login jest już wykorzystywany rzucany jest wyjątek: @ref common::exception::RegistrationException()
 * @param user użtywkonik (login i hasło)
 * @retrun użytkownik o uzuepłnionych wszyskich podstawowych polach*/
			common::User registrationUser(common::User user);

/** Metoda sprawdzajaca czy użytkownik podał przy logowaniu poprawne dane. Gdy zalogowanie niepoprawne rzucany jest wyjątek @ref common::exception::ValidateUserException();. Wywołuje @ref validateUser(common::User user)
 * @param login login
 * @param passed hasło
 * @retrun użytkownik o uzuepłnionych wszyskich podstawowych polach*/
			common::User validateUser(std::string login, std::string passwd);
/** Metoda sprawdzajaca czy użytkownik podał przy logowaniu poprawne dane. Gdy zalogowanie niepoprawne rzucany jest wyjątek @ref common::exception::ValidateUserException()
 * @param user użtywkonik (hasło i login)
 * @retrun użytkownik o uzuepłnionych wszyskich podstawowych polach*/
			common::User validateUser(common::User user);


		private:
			log4cpp::Category &logger_ = log4cpp::Category::getInstance(std::string("server.service"));  /**<  Zmienna pomocnicza służąca do logowania zdarzeń*/


		};

	}
}
#endif //BACKUPER_USER_SERVICE_H
