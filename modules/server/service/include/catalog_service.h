/**
* @file catalog_service.h
* @brief Plik nagłówkowy klasy CatalogService.
* @author Marta Mikołajczyk
*/
#ifndef BACKUPER_CATALOG_SERVICE_H
#define BACKUPER_CATALOG_SERVICE_H

#include <string>
#include <iostream>
#include <log4cpp/Category.hh>
#include "user.h"
#include "catalog.h"
#include "user_service.h"
#include "connection_provider.h"
#include <soci.h>
#include <postgresql/soci-postgresql.h>
#include <utility>
#include <exception/clientdecision_exception.h>

namespace server {
	namespace service {
/**Klasa CatalogService realizująca logikę biznesową związaną z obsługą katalogów.*/
		class CatalogService {
			typedef std::pair<ClientDecision, common::Catalog> actionOnCatalog;

		public:
/** Metoda dodająca katalog do bazy danych. Wywołuje metodę @ref addCatalog(ClientDecision client_decision, common::Catalog catalog).
 * @param client_decision dezcyja co należy zrobić z danym katalogiem (ADD, REPLACE)
 * @param parent katalog do którego będzie dodawany katalog
 * @@param id użytkownika w kontekście którego wykonywane są operacje
 * */
			long long int addCatalog(ClientDecision client_decision, std::string name, long long int parent,
									 long long int id_user);

/** Metoda dodająca katalog do bazy danych. Wywołuje metodę @ref addCatalog(common::Catalog catalog);.
 * @param client_decision dezcyja co należy zrobić z danym katalogiem (ADD, REPLACE)
 * @param catalog katalog uzupełniony o cały konetkst pracy, czyli o pola name_, owner_, parent_
 * */
			long long int addCatalog(ClientDecision client_decision, common::Catalog catalog);

/** Metoda dodająca katalogi do bazy danych. Wywołuje metodę @ref addCatalog(ClientDecision client_decision, common::Catalog catalog);.
 * @param catalogs werktor składający się z par client_decision i katalog uzupełniony o cały potrzebny kontekst
 * */
			std::vector<long long int> addCatalogs(std::vector<std::pair<ClientDecision, common::Catalog>> &catalogs);

/** Metoda dodająca katalogi do bazy danych. Wywołuje metodę @ref addCatalog(common::Catalog catalog).
 * @param catalogs werktor składający się z katalogów które będą dodane do bazy danych. Ważne - muszą mieć uzupełniony cały kontekst.
 * */
			std::vector<long long int> addCatalogs(std::vector<common::Catalog> &catalogs);

/** Metoda uzupełniająca wszystkie podstawowe informacje o katalogu na postawie jego id.
 * @param catalog katalog który jest uzupełniany*/
			void completeCatalogInformation(common::Catalog &catalog);

/** Metoda zwracająca id katalogu o podanych parametrach
 * @param name nazwa katalogu
 * @param położenie kalaogu, jego parent
 * @param id_user użytkownik do którego należy katalog
 * @retrun id poszukiwanego katalogu
 * */
			long giveIdCatalog(std::string name, long long int parent, long long int id_user);

/** Metoda zwracająca id katalogu o podanych parametrach
 * @param catalog musi zawierać pola: name_, parent_, owner_;
 * @retrun id poszukiwanego katalogu
 * */
			long giveIdCatalog(common::Catalog catalog);

/** Metoda uzupełniająca dany vektor katalogów o wszyskie katalogi znajdujące się we wskazanym katalogu.
 * @param id_catalog katalog w którym szukamy
 * @param wektor katalogów znajdujący się w podanym
 * */
			void getListOfCatalogs(long long int id_catalog, std::vector<common::Catalog> &catalogs);

/** Metoda uzupełniająca dany vektor katalogów o wszyskie katalogi znajdujące się we wskazanym katalogu.
 * @param catalog  katalog w którym szukamy (musi mieć uzupełnione id)
 * @param wektor katalogów znajdujący się w podanym
 * */
			void getListOfCatalogs(common::Catalog parent, std::vector<common::Catalog> &catalogs);

/** Metoda sprawdzająca czy użytkownik ma katalog o takiej nazwie w danym katalogu
 * @param name nazwa katalogu
 * @param parent id katalogu w którym znajduje się szukany
 * @param id_user id użytkownika dla którego wykonywana jest akcja
 * @return true- isteniej katalog o takiej nazwie, w takiej lokalizacji dla tego użytkownika
 * */
			bool isSuchCatalog(std::string name, long long int parent, long long int id_user);

/** Metoda usuwająca katalog z bazy danych oraz pliki znajdujące się w nim. Usuwa z bazy danych oraz fizyczną reprezentacje
 * @param id_catalog id katalogu który ma zostać usunięty*/
			void removeCatalogWithContent(long long int id_catalog);

/** Metoda usuwająca katalog z bazy danych oraz pliki znajdujące się w nim. Usuwa z bazy danych oraz fizyczną reprezentacje
 * @param catalog katalogu który ma zostać usunięty. Koniecznie musi zawierać swoje id*/
			void removeCatalogWithContent(common::Catalog catalog);

		private:
			log4cpp::Category &logger_ = log4cpp::Category::getInstance(std::string("server.service"));/**<  Zmienna pomocnicza służąca do logowania zdarzeń*/

/** Metoda dodająca katalog do bazy danych. Musi on mieć komplente wszystkie pola
 * @param catalog dodawany katalog*/
			long long int addCatalog(common::Catalog catalog);


		};

	}
}
#endif //BACKUPER_CATALOG_SERVICE_H
