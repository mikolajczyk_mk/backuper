//
// Created by ruda on 06.06.16.
//


#include <gtest/gtest.h>
#include <controller.h>
#include <user_service.h>
#include "config.h"
#include "md5.h"
#include <string>
#include <chrono>
#include <ctime>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>

class UserServiceTest : public ::testing::Test {
public:
	UserServiceTest(){
		common::Config::prepareConfiguration("./config_test.txt");
	}
};



TEST_F(UserServiceTest,PositiveChceckIsLoginInDatabaseTest) {
	//common::Config::prepareConfiguration("./config.txt");
	server::service::UserService user_service;
	ASSERT_EQ(user_service.isLoginInDatabase("admin"),true);

}

TEST_F(UserServiceTest, NegativeChceckIsLoginInDatabaseTest) {
	server::service::UserService user_service;
	ASSERT_EQ(user_service.isLoginInDatabase("Królowa Karolina"), false);
//ASSERT_EQ(-1.0, primitiveFunction(-0.2));
}

TEST_F(UserServiceTest,PositiveValidateUserTest) {
	server::service::UserService user_service;
	ASSERT_EQ(user_service.validateUser("user2",common::md5("user2")).getId_(),2);

}

TEST_F(UserServiceTest, NegativeValidateUserTest) {
	server::service::UserService user_service;
	ASSERT_NE(user_service.validateUser("user2",common::md5("user2")).getId_(),1);
}

TEST_F(UserServiceTest,PositiveRegistrationTest) {
	server::service::UserService user_service;
//Generowanie loginów które się nie powtórzą za pomocą obecnej daty
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	std::time_t now_c = std::chrono::system_clock::to_time_t(now);
	std::string login = std::asctime(std::localtime(&now_c));

	std::string passwd = common::md5("asdf");
	long long int id = user_service.registrationUser(login,passwd).getId_();
	boost::filesystem::path path(common::Config::getLocationMasterRoot_() + std::to_string(id));
	bool is_directory = boost::filesystem::is_directory(path);
	ASSERT_EQ(is_directory && user_service.isLoginInDatabase(login),true);

}

