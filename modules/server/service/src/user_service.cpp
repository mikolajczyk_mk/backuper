//
// Created by ruda on 29.05.16.
//


#include "user_service.h"
#include "file_orm.hpp"
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <exception/validateuser_exception.h>
#include <exception/registration_exception.h>
#include "connection_provider.h"

namespace server {
	namespace service {

		typedef std::unique_ptr<soci::session> PSession;
		//todo:make it from file
		long long ID_ROOT_MASTER = 1;


		common::User  UserService::addNewUser(std::string login, std::string passwd) {
			return addNewUser(common::User(login, passwd));

		}


		common::User UserService::addNewUser(common::User user) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();

			long id;
			sql->get_next_sequence_value("users_seq", id);
			user.setId_(id);

			common::Catalog catalog;
			sql->get_next_sequence_value("catalogs_seq", id);
			catalog.setId_(id);
			//catalog.setName_(common::md5(user.getLogin_()));
			catalog.setName_(std::to_string(user.getId_()));
			catalog.setParent_(ID_ROOT_MASTER);
			catalog.setOwner_(user.getId_());
			user.setRoot_(catalog.getId_());

			logger_.debug("[UserService::addNewUser] " + user.user_to_string() + " " + catalog.catalog_to_string());

			soci::transaction tr(*sql);
			*sql << "insert into users(id,login,passwd) values(:id, :login, :passwd)", soci::use(user);
			*sql << "insert into catalogs(id,name,parent,owner) values(:id, :name, :parent, :owner)", soci::use(
					catalog);
			*sql << "update users set root = :root where id = :id", soci::use(user);
			tr.commit();

			boost::filesystem::path path(common::Config::getLocationMasterRoot_() + catalog.getName_());
			logger_.debug("[UserService::addNewUser] Path where root catalog of user is created: " + path.string());

			boost::filesystem::create_directory(path);
			return user;
		}


		void UserService::changePasswdWithOldPasswd(std::string login, std::string passwd, std::string new_passwd) {
			changePasswdWithOldPasswd(common::User(login, passwd), new_passwd);

		}


		void UserService::changePasswdWithOldPasswd(common::User user, std::string new_passwd) {
			try {
				validateUser(user);
				user.setPasswd_(new_passwd);
				changePasswdWithoutOldPasswd(user);
			} catch (common::exception::ValidateUserException &vue) {
				logger_.info("[UserService, changePasswdWithOldPasswd] Login; %s", vue.what());
				throw;
			}
			catch (...) {
				logger_.info("[UserService, changePasswdWithOldPasswd] Login; Something goes wrong...");
				throw;
			}
		}

		void UserService::changePasswdWithoutOldPasswd(common::User user) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			*sql << "update users set passwd=:passwd where login = :login", soci::use(user);
			logger_.debug("[UserService::changePasswdWithoutOldPasswd] New passwd: " + user.getPasswd_());
		}

		bool UserService::isLoginInDatabase(const std::string &login) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			*sql << "select * from users where login=:lg", soci::use(login, "lg");
			logger_.debug("[UserService::isLoginInDatabase] Is: " + std::to_string(sql->got_data()));
			return sql->got_data();
		}



		common::User UserService::registrationUser(std::string login, std::string passwd) {
			return registrationUser(common::User(login, passwd));
		}

		common::User UserService::registrationUser(common::User user) {
			logger_.debug("[UserService, registrationUser] " + user.user_to_string());
			std::string message = "Użytkownik o takim loginie istnieje!";
			bool isLogin = isLoginInDatabase(user.getLogin_());
			//logger_.debug("[UserService, registrationUser] Is login in database: " + std::to_string(isLogin));
			if (!isLogin) {
				return addNewUser(user);
			}
			else {
				throw common::exception::RegistrationException();
			}
			return common::User(0);
		}


		common::User UserService::validateUser(std::string login, std::string passwd) {
			return validateUser(common::User(login, passwd));
		}

		common::User UserService::validateUser(common::User user) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::User user1;
			*sql << "select * from users where login=:login and passwd=:passwd", soci::use(user), soci::into(user1);
			logger_.debug("[UserService, validateUser] Parameters to database: " + user1.user_to_string());
			if (!sql->got_data()) {
				throw common::exception::ValidateUserException();
			}
			return user1;
		}


	}
}