//
// Created by ruda on 30.05.16.
//

#include <client_decision.h>
#include <file_service.h>
#include "catalog_service.h"
#include "file_orm.hpp"
#include "client_decision.h"
#include <utility>


namespace server {
	namespace service {
		typedef std::unique_ptr<soci::session> PSession;
		typedef std::pair<ClientDecision, common::Catalog> actionOnCatalog;



		//todo: tu powinno mówić czy się udało utworzyć - może jakiś wyjątek rzucać?
		long long int CatalogService::addCatalog(common::Catalog catalog) {

			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			long id;
			sql->get_next_sequence_value("catalogs_seq", id);
			catalog.setId_(id);
			*sql << "insert into catalogs(id,name,parent,owner) values(:id, :name, :parent, :owner)", soci::use(
					catalog);
			logger_.debug("[CatalogService, addCatalog] Added to database "+ catalog.catalog_to_string());
			return catalog.getId_();

		};

		long long int CatalogService::addCatalog(ClientDecision client_decision, std::string name, long long parent,
										long long id_user) {
			common::Catalog catalog;
			catalog.setName_(name);
			catalog.setParent_(parent);
			catalog.setOwner_(id_user);
			return addCatalog(client_decision, catalog);
		};

		long long int CatalogService::addCatalog(ClientDecision client_decision, common::Catalog catalog) {
			logger_.debug("[CatalogService, addCatalog] client decision: "+  std::to_string(client_decision));

			switch (client_decision) {
				case ADD:
					return addCatalog(catalog);

				case REPLACE:
					catalog.setId_(giveIdCatalog(catalog));
					removeCatalogWithContent(catalog);
					return addCatalog(catalog);
				default:
					throw common::exception::ClientDecisionException("ADD and REPLACE");
			}
		}

		std::vector<long long int> CatalogService::addCatalogs(std::vector<actionOnCatalog> &catalogs) {
			std::vector<long long int> catalogs_ids;
			for (auto catalog : catalogs) {
				catalogs_ids.push_back(addCatalog(catalog.first, catalog.second));
			}
			return catalogs_ids;
		}

		std::vector<long long int> CatalogService::addCatalogs(std::vector<common::Catalog> &catalogs) {
			std::vector<long long int> catalogs_ids;
			for (auto catalog : catalogs) {
				catalogs_ids.push_back(addCatalog(catalog));
			}
			return catalogs_ids;
		}

		void CatalogService::completeCatalogInformation(common::Catalog &catalog){
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			*sql<< "select * from catalogs where id=:id", soci::use(catalog.getId_(),"id"), soci::into(catalog);
			logger_.debug("[CatalogService, completeCatalogInformation] " + catalog.catalog_to_string());
		};



		long CatalogService::giveIdCatalog(std::string name, long long parent, long long id_user) {
			return giveIdCatalog(common::Catalog(name, parent, id_user));

		}


		long CatalogService::giveIdCatalog(common::Catalog catalog) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			long id;
			*sql << "select id from catalogs where name=:name and parent=:parent and owner=:owner", soci::use(
					catalog), soci::into(id);
			logger_.debug("[CatalogService, giveIdCatalog] Id catalog: "+  std::to_string(id));
			return id;

		}

		void CatalogService::getListOfCatalogs(long long int id_catalog, std::vector<common::Catalog> &catalogs) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::Catalog catalog;
			*sql << "select * from catalogs where id=:id_catalog", soci::use(id_catalog, "id_catalog"), soci::into(
					catalog);
			getListOfCatalogs(catalog, catalogs);
		}

		void CatalogService::getListOfCatalogs(common::Catalog parent, std::vector<common::Catalog> &catalogs) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::Catalog catalog;
			logger_.debug("[CatalogService, getListOfCatalogs] Parent: "+  parent.catalog_to_string());
			soci::statement st = (sql->prepare << "select * from catalogs where parent=:id", soci::use(
					parent), soci::into(catalog));
			st.execute();
			while (st.fetch()) {
				catalogs.push_back(catalog);
				logger_.debug("[CatalogService, getListOfCatalogs] Chlidren : "+  catalog.catalog_to_string());
			}


		}

		bool CatalogService::isSuchCatalog(std::string name, long long parent, long long id_user) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::Catalog catalog(name, parent, id_user);
			*sql << "select * from catalogs where name=:name and parent=:parent and owner=:owner", soci::use(catalog);
			logger_.debug("[CatalogService, isSuchCatalog] Is: " + std::to_string(sql->got_data()) +" " + catalog.catalog_to_string());

			return sql->got_data();

		}

//todo: popraw to bo jest duża nadmiarowość kodu...
		void CatalogService::removeCatalogWithContent(common::Catalog catalog) {
			logger_.debug("[CatalogService, removeCatalogWithContent] "+ catalog.catalog_to_string());
			FileService fileService;
			fileService.removeAllFileFromCatalog(catalog, true);
		}

		void CatalogService::removeCatalogWithContent(long long id_catalog) {
			logger_.debug("[CatalogService, removeCatalogWithContent] Id catalog: "+std::to_string(id_catalog));
			FileService fileService;
			fileService.removeAllFileFromCatalog(id_catalog, true);
		}


	}
}