//
// Created by ruda on 31.05.16.
//



#include "file_service.h"
#include "file_orm.hpp"
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <utility>
#include <algorithm>
#include <catalog_service.h>
#include <exception/clientdecision_exception.h>

namespace server {
	namespace service {

		typedef std::unique_ptr<soci::session> PSession;
		typedef std::pair<ClientDecision, common::File> actionOnFile;


		//szybkie sprawdzenie stąd limit
		bool FileService::areVersionsOfFile(common::File file) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			*sql << "select id from files where owner=:owner and version=:version and id!=version limit 1", soci::use(file.getOwner_(),
																									  "owner"), soci::use(
					file.getId_(), "version");
			//		soci::statement statement = (sql->prepare <<"insert into users(login,passwd) values(:lg, :pw)", soci::use(login, "lg"), soci::use(
			//passwd, "pw"));
			logger_.debug("[FileService,areVersionsOfFile] Is at least one version of file: " + std::to_string(sql->got_data()));
			return sql->got_data();

		}

		long long int FileService::addFile(ClientDecision client_decision, std::string file_name, long long id_catalog,
										   long long id_user) {

			return addFile(client_decision, common::File(file_name, id_catalog, id_user));

		}

		long long int FileService::addFile(ClientDecision client_decision, common::File file) {
			logger_.debug("[FileService,addFile] client decision: " + std::to_string(client_decision)+ " "+file.file_to_string());
			switch (client_decision) {
				case ADD:
					return addNewFile(file);


				case VERSION:
					//tu użytkownik nie będzie miał możliwości zmiany nazyw pliku.
					return addNewVersion(file, selectIdLastVersionFile(file), 1);

				case REPLACE:
					removeAllVersion(client_decision, file);
					return addNewFile(file);
				default:
					throw common::exception::ClientDecisionException("ADD, VERSION and REPLACE");

			}
		}


		long long int FileService::addFile(std::pair<ClientDecision, common::File> pair) {
			return addFile(pair.first, pair.second);
		}

		std::vector<long long int> FileService::addFiles(std::vector<actionOnFile> action_on_files) {
			std::vector<long long int> files_id;
			for (auto &item: action_on_files) {
				files_id.push_back(addFile(item.first, item.second));


			}

			return files_id;
		}

		long long int FileService::addNewFile(common::File new_file) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			setIdAndVersion(new_file, sql);
			new_file.setFilesystemName_(std::to_string(new_file.getId_()));
			*sql <<
			"insert into files(id,name,in_filesystem_name,version,parent,owner) values(:id, :name, :in_filesystem_name, :version, :parent,:owner) ", soci::use(
					new_file);
			logger_.debug("[FileService,addNewFile] File parameters insert to database: " + new_file.file_to_string());
			return new_file.getId_();

		}

		long long int FileService::addNewVersion(common::File file, long long id_old_version, bool set_old_name) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::File old_version;
			*sql << "select * from files where id=:old", soci::use(id_old_version, "old"), soci::into(old_version);

			file.setParent_(old_version.getParent_());
			file.setOwner_(old_version.getOwner_());

			if (set_old_name) {
				file.setName_(old_version.getName_());
			}

			return addNewVersion(file, old_version);


		}


//założenie że old_version to ma już wszystkie dane tak jak i old_version, Zakładam że jak użytkownik chce dodać wersję wersji pliku to dodaje na szczyt. Nie tworzymy rozgałęzień.
		long long int FileService::addNewVersion(common::File new_version, common::File old_version) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			setIdAndVersion(new_version, sql);
			new_version.setFilesystemName_(std::to_string(new_version.getId_()));
			soci::transaction tr(*sql);
			*sql <<
			"insert into files(id,name,in_filesystem_name,version,parent,owner) values(:id, :name, :in_filesystem_name, :version, :parent,:owner) ", soci::use(
					new_version);
			*sql << "update files set version=:new where version=:version and owner=:owner", soci::use(
					new_version.getId_(), "new"), soci::use(old_version);
			tr.commit();

			logger_.debug("[FileService,addNewVersion] File parameter insert to database: " + new_version.file_to_string());
			return new_version.getId_();

		}

		long long int FileService::addNewVersionWithNewName(std::string new_name, long long id_old_version) {

			return addNewVersion(common::File(new_name), id_old_version, 0);
		}
//todo: żeby ten plik miał nazwę r

		long long int FileService::addNewVersionWithoutChangeName(long long int id_old_version) {
			common::File file(id_old_version);
			completeFileInformation(file);
			return addNewVersion(file, file);
		}

		void FileService::addInformationSizeAndLastTimeWrite(common::File &file) {
			boost::filesystem::path path(common::Config::getLocationMasterRoot_() + std::to_string(file.getOwner_()) +
										 common::Config::getPathSeparator_() + file.getFilesystemName_());
			file.setSize_(boost::filesystem::file_size(path));
			std::time_t t = boost::filesystem::last_write_time(path);
			file.setLastWriteTime_(std::asctime(std::localtime(&t)));
		}

		void FileService::addInformationOfNumberOfVersion(common::File &file) {
			int numberOfVersion_ =0;
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			*sql << "select count(*) from files where version=:version", soci::use(file), soci::into(numberOfVersion_);
			file.setNumberOfVersion_(numberOfVersion_);
		}

		void FileService::completeFileInformation(common::File &file) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			*sql << "select * from files where id=:id", soci::use(file.getId_(), "id"), soci::into(file);
			logger_.debug("[FileService, completeFileInformation] "+file.file_to_string());
		}


		void FileService::getAllFilesAndVersion(long long id_catalog, std::vector<common::File> &files) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::Catalog catalog;
			*sql << "select * from catalogs where id=:id_catalog", soci::use(id_catalog, "id_catalog"), soci::into(
					catalog);
			logger_.debug("[FileService, getAllFilesAndVersion] " + catalog.catalog_to_string());
			getAllFilesAndVersion(catalog, files);
		}

		void FileService::getAllFilesAndVersion(common::Catalog catalog, std::vector<common::File> &files) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::File file;

			soci::statement st = (sql->prepare << "select * from files where parent=:id", soci::use(
					catalog), soci::into(file));
			st.execute();
			while (st.fetch()) {
				files.push_back(file);
				logger_.debug("[FilesService, getAllFilesAndVersion ] From Catalog: " + file.file_to_string());
			}

		}

		void FileService::getAllVersionOfFile(long long id_file, std::vector<common::File> &files) {
			common::File file(id_file);
			completeFileInformation(file);
			getAllVersionOfFile(file, files);
		}

//ważne plik ma swoje id.
		void FileService::getAllVersionOfFile(common::File file, std::vector<common::File> &files) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::File version;
			logger_.debug("[FileService,getAllVersionOfFile] From: " + file.file_to_string());
			soci::statement st = (sql->prepare << "select * from files where version=:version", soci::use(
					file), soci::into(version));
			st.execute();
			while (st.fetch()) {
				files.push_back(version);
				logger_.debug("[FileService,getAllVersionOfFile] Version: " + version.file_to_string());
			}
		}


		void FileService::getLastVersionFilesFromCatalog(long long id_catalog, std::vector<common::File> &files) {
			common::Catalog catalog(id_catalog);
			CatalogService catalog_service;
			catalog_service.completeCatalogInformation(catalog);
			getLastVersionFromCatalog(catalog, files);
		}

		void FileService::getLastVersionFromCatalog(common::Catalog catalog, std::vector<common::File> &files) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			common::File file;
			logger_.debug("[FileService,getLastVersionFromCatalog] "+ catalog.catalog_to_string());
			soci::statement st = (sql->prepare << "select * from files where parent=:id and version=id", soci::use(
					catalog), soci::into(file));
			st.execute();
			while (st.fetch()) {
				files.push_back(file);
				logger_.debug("[FileService,getLastVersionFromCatalog] "+file.file_to_string());
			}
		}

		long long int FileService::givePenultimateVersion(common::File &file) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			long long id;
			*sql << "select id from files where id!=:version and version=:version order by id DESC limit 1", soci::use(
					file), soci::into(id);
			logger_.debug("[FileService,givePenultimateVersion] Id: " + std::to_string(id));
			return id;
		}


		bool FileService::isSuchFile(std::string file_name, long long id_catalog, long long id_user) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			*sql <<
			"select * from files where owner=:user and parent=:id_catalog and name=:file_name limit 1", soci::use(
					id_user, "user"), soci::use(id_catalog, "id_catalog"), soci::use(file_name, "file_name");
			logger_.debug("[FileService, isSuchFile] " + std::to_string(sql->got_data()));
			return sql->got_data();
		}

		//tutaj chyab powinnam rzucać jakiś wyjątek.
		long long int FileService::selectIdLastVersionFile(common::File new_file) {
			PSession sql = db::ConnectionProvider::getInstance().getConncetion();
			long long int last_version;
			*sql <<
			"select id from files where owner=:owner and parent=:parent and name=:name and version=id", soci::use(
					new_file), soci::into(last_version);
			logger_.debug("[FileService, selectIdLastVersionFile] ID:  " + std::to_string(last_version));
			return last_version;
		}


		void FileService::setIdAndVersion(common::File &file, PSession &sql) const {
			long id;
			sql->get_next_sequence_value("files_seq", id);
			file.setId_(id);
			file.setVersion_(id);
			logger_.debug("[FileService, setIdAndVersion] " + file.file_to_string());
		}

		void FileService::removeAllFileFromCatalog(long id_catalog, bool delete_with_catalog) {
			common::Catalog catalog(id_catalog);
			CatalogService catalog_service;
			catalog_service.completeCatalogInformation(catalog);
			removeAllFileFromCatalog(catalog, delete_with_catalog);
		}


		void FileService::removeAllFileFromCatalog(common::Catalog catalog, bool delete_with_catalog) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			logger_.debug("[FileService, removeAllFileFromCatalog] " + catalog.catalog_to_string());
			std::vector<std::string> files_system_names;
			std::string file_system_name;
			soci::statement st = (sql->prepare << "select in_filesystem_name from files where parent=:id", soci::use(
					catalog.getId_(), "id"), soci::into(file_system_name));
			st.execute();
			while (st.fetch()) {
				files_system_names.push_back(file_system_name);
			}


			soci::transaction tr(*sql);
			*sql << "DELETE FROM files WHERE id IN (select id from files where parent=:parent);", soci::use(
					catalog.getId_(), "parent");
			if (delete_with_catalog) {
				*sql << "delete from catalogs where id =:id", soci::use(catalog);
			}
			tr.commit();
			for (auto &file_name: files_system_names) {
				std::cout << "Nazwy usuwanych plików " << file_name << std::endl;
				boost::filesystem::path path(
						common::Config::getLocationMasterRoot_() + std::to_string(catalog.getOwner_()) +
						common::Config::getPathSeparator_() + file_name);
				logger_.debug("[FileService, removeAllFileFromCatalog] Path from file will be deleted: " + path.string());
				logger_.debug("[FileService, removeAllFileFromCatalog] Deleted file (filesystem name): " + file_name);

				boost::filesystem::remove(path);
			}


		}

		void FileService::removeAllVersion(ClientDecision client_decision, common::File file) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			//
			long version;
			logger_.debug("[FileService,removeAllVersion] client_decision: " + std::to_string(client_decision)+ "Basic file: " + file.file_to_string());
			if (client_decision == ALL) {
				version = file.getVersion_();
			}
			else {
				version = selectIdLastVersionFile(file);
			}

			std::vector<std::string> files_system_names;
			std::string file_system_name;
			soci::statement st = (sql->prepare <<
								  "select in_filesystem_name from files where version=:version", soci::use(
					version, "version"), soci::into(file_system_name));
			st.execute();
			while (st.fetch()) {
				files_system_names.push_back(file_system_name);
			}
//todo:: version teraz to jest id pliku
			*sql << "DELETE FROM files WHERE id IN (select id from files where version=:version);", soci::use(version,
																											  "version");


			for (auto &file_name: files_system_names) {
				boost::filesystem::path path(
						common::Config::getLocationMasterRoot_() + std::to_string(file.getOwner_()) +
						common::Config::getPathSeparator_() + file_name);
				logger_.debug("[FileService, removeAllVersion] Path from file will be deleted: " + path.string());
				logger_.debug("[FileService, removeAllVersion] Deleted file (filesystem name): " + file_name);

				boost::filesystem::remove(path);
			}

		}


		void FileService::removeFile(ClientDecision client_decision, long long id) {
			removeFile(client_decision, common::File(id));
		}


		//to w sumie głupia funkcja
		void FileService::removeFile(ClientDecision client_decision, long long id, common::User user) {
			removeFile(client_decision, common::File(id, user.getId_()));

		}

		void FileService::removeFile(ClientDecision client_decision, long long id, long long id_user) {

			removeFile(client_decision, common::File(id, id_user));

		}

		void FileService::removeFile(ClientDecision client_decision, common::File file) {
			completeFileInformation(file);
			logger_.debug("[FileService, removeFile] client decision: "+  std::to_string(client_decision) + " "+ file.file_to_string());
			switch (client_decision) {
				case ONE:
					removeSelectedFile(file);
					break;

				case ALL:
					removeAllVersion(client_decision, file);
					break;

				default:
					throw common::exception::ClientDecisionException("ALL and ONE");
			}

		}

		void FileService::removeFile(std::pair<ClientDecision, common::File> file_with_decision) {
			removeFile(file_with_decision.first, file_with_decision.second);
		}

		void FileService::removeFile(std::vector<actionOnFile> action_on_files) {
			for (auto &file_with_decision: action_on_files)
				removeFile(file_with_decision);
		}


		void FileService::removeSelectedFile(common::File file) {
			PSession sql = server::db::ConnectionProvider::getInstance().getConncetion();
			soci::transaction tr(*sql);
			long long int id = givePenultimateVersion(file);
			*sql << "update files set version=:new where version=:id and owner=:owner", soci::use(id, "new"), soci::use(
					file);
			*sql << "delete from files where id=:id and owner=:owner", soci::use(file);
			tr.commit();
			//todo: sprawdź czy na pewno działa pod win.

			logger_.debug("[FileService, removeSelectedFile] Deleted file : " + file.file_to_string());
			boost::filesystem::path path(common::Config::getLocationMasterRoot_() + std::to_string(file.getOwner_()) +
										 common::Config::getPathSeparator_() + file.getFilesystemName_());
			logger_.debug("[FileService, removeSelectedFile] Path from file will be deleted: " + path.string());
			boost::filesystem::remove(path);
		}

	}
}
