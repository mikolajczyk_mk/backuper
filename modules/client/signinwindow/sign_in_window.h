/**
* @file sign_in_window.h
* @brief Plik nag��wkowy klasy SignInWindow.
* @author Krzysztof Kachniarz
*/

#ifndef SIGNINWINDOW_H
#define SIGNINWINDOW_H

#include <QWidget>

namespace Ui {
class SignInWindow;
}
/**Klasa odpowiedzialna za okienko logowania u�ytkownika*/
class SignInWindow : public QWidget
{
    Q_OBJECT

public:
    explicit SignInWindow(QWidget *parent = 0);
    ~SignInWindow();

private slots:
	/**Metoda s�u��ca do obs�ugi zdarzenia wci�ni�cia przycisku "OK".*/
    void on_okButton_clicked();
	/**Metoda s�u��ca do obs�ugi zdarzenia wci�ni�cia przycisku "Back".*/
    void on_backButton_clicked();

private:
    Ui::SignInWindow *ui;
};

#endif // SIGNINWINDOW_H
