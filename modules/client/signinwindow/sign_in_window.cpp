#include "sign_in_window.h"
#include "ui_sign_in_window.h"
#include "main_window.h"
#include "welcome_window.h"
#include <string>
#include "connector.h"
#include <md5.h>



#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <iostream>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#elif __linux__
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <zconf.h>

//void closesocket(int socket) { close(socket); }

#endif


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT 27015
#define DEFAULT_ADDRESS "127.0.0.1"

SignInWindow::SignInWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SignInWindow)
{
    ui->setupUi(this);
	ui->okButton->setFocus();
}

SignInWindow::~SignInWindow()
{
    delete ui;
}

void SignInWindow::on_okButton_clicked()
{
	QString qLogin = ui->loginLineEdit->text();
	QString qPassword = ui->passwordLineEdit->text();
	if ((qLogin != "") && (qPassword != "")){
		std::string login = qLogin.toStdString();
		std::string password = common::md5(qPassword.toStdString());

		//const char *login = test_str.c_str();
		//std::cout << "login: " << login << std::endl;
		Connector* connector = &Connector::getInstance();
		int result = connector->logIn(login, password, true);
		if (result == 0)
			std::cout << "Server dissapeard " << std::endl;
		else if (result == 1){
			//auto main_window = std::make_shared<MainWindow>();
			MainWindow *main_window = new MainWindow();
			main_window->setWindowTitle("Backuper");
			main_window->show();
			this->close();
			delete this;
		}
		else
		{
			this->ui->errorLabel->setText("Incorrect login or password");
		}
	}
	else{
		this->ui->errorLabel->setText("Please insert login and password");
	}

}

void SignInWindow::on_backButton_clicked()
{
	welcomeWindow *_welcomeWindow = new welcomeWindow();
	_welcomeWindow->setWindowTitle("Welcome");
	_welcomeWindow->show();
	this->close();
	delete this;
}