#include "main_window.h"
#include "ui_main_window.h"
#include <QFileDialog>
#include <QListView>
#include <QTreeView>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "connector.h"
#include <boost/filesystem.hpp>
#include <vector>
#include <QApplication>
#include <QDesktopWidget>
#include <QCoreApplication>
#include <QHeaderView>
#include <QMessageBox>
#include <QtWidgets>

using namespace boost::filesystem;

MainWindow::MainWindow(QWidget *parent) : QWidget(parent), ui(new Ui::MainWindow)
{
	refreshTable();
	connect(table_widget, SIGNAL(cellClicked(int, int)), this, SLOT(cellClicked(int, int)));
}

void MainWindow::refreshTable(){
	std::cout << "refreshing table... " << std::endl;
	Connector *connector = &Connector::getInstance();
	files_ = connector->getFileList(id_folder_);
	catalogs_ = connector->getCatalogList(id_folder_);
	ui->setupUi(this);
	table_widget = new QTableWidget(this->ui->tableWidget);
	table_widget->setRowCount(files_.size() + catalogs_.size());
	table_widget->setColumnCount(4);
	table_header << "Type" << "Name" << "Size" << "Version";
	table_widget->setHorizontalHeaderLabels(table_header);
	table_widget->verticalHeader()->setVisible(false);
	table_widget->setEditTriggers(QAbstractItemView::NoEditTriggers);
	table_widget->setSelectionBehavior(QAbstractItemView::SelectRows);
	table_widget->setSelectionMode(QAbstractItemView::SingleSelection);
	table_widget->setShowGrid(true);
	table_widget->setStyleSheet("QTableView {selection-background-color: red;}");
	table_widget->setGeometry(QApplication::desktop()->screenGeometry());
	int i = 0;
	for (common::Catalog row : catalogs_){
		table_widget->setItem(i, 0, new QTableWidgetItem(QString("d")));
		table_widget->setItem(i, 1, new QTableWidgetItem(QString(row.getName_().c_str())));
		++i;
	}
	for (common::File row : files_){
		table_widget->setItem(i, 0, new QTableWidgetItem(QString("f")));
		table_widget->setItem(i, 1, new QTableWidgetItem(QString(row.getName_().c_str())));
		table_widget->setItem(i, 2, new QTableWidgetItem(QString((std::to_string(row.getSize_()).c_str()))));
		table_widget->setItem(i, 3, new QTableWidgetItem(QString((std::to_string(row.getNumberOfVersion_()).c_str()))));
		++i;
	}
	QScrollBar * poScrollBar = new QScrollBar(table_widget);
	poScrollBar->setAutoFillBackground(true);
	poScrollBar->setHidden(false);
	poScrollBar->show();
	poScrollBar->setHidden(false);
	//poScrollBar->setShown(true); 
	poScrollBar->show();
	poScrollBar->showNormal();
	table_widget->resizeColumnsToContents();
	table_widget->horizontalHeader()->setStretchLastSection(true);
	//table_widget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch); 

	table_widget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	table_widget->verticalScrollBar()->setDisabled(false);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_downloadButton_clicked()
{
	if (selected_row_ == -1)
		return;

	std::cout << "download button " << std::endl;
	QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "C:", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	Connector *connector = &Connector::getInstance();
	std::string name;
	if (selected_row_ < catalogs_.size())
	{
		common::Catalog catalog = catalogs_[selected_row_];
		std::cout << "choosen: " << catalog.getName_() << std::endl;
		name = dir.toStdString() + "/" + catalog.getName_();
		connector->downloadCatalog(true, catalog.getId_(), name);
	}
	else
	{
		common::File file = files_[selected_row_ - catalogs_.size()];
		std::cout << "choosen: " << file.getName_() << std::endl;
		name = dir.toStdString() + "/" + file.getName_();
		FILE *file_handle = fopen(name.c_str(), "wb");
		if (file_handle != NULL)
			connector->downloadFile(true, file.getId_(), file_handle);
	}

	std::cout << "name: " << name << std::endl;

}

void MainWindow::on_deleteButton_clicked(){
	Connector *connector = &Connector::getInstance();
	if (selected_row_ < catalogs_.size())
	{
		common::Catalog catalog = catalogs_[selected_row_];
		std::cout << "choosen: " << catalog.getName_() << std::endl;
		connector->removeCatalog(catalog.getId_());
	}
	else
	{
		common::File file = files_[selected_row_ - catalogs_.size()];
		std::cout << "choosen: " << file.getName_() << std::endl;
		connector->removeFile(file.getId_());
	}

}


void MainWindow::on_uploadFilesButton_clicked()
{
	std::cout << "on upload button clicked" << std::endl;
	QStringList file_names = QFileDialog::getOpenFileNames(this, tr("Choose files"), "C://", "All files (*.*)");

	Connector *connector = &Connector::getInstance();
	for (QString file_name : file_names){
		std::cout << "on upload button clicked file: " << file_name.toStdString() << std::endl;
		FILE *file_handle = fopen(file_name.toStdString().c_str(), "rb");
		if (file_handle != NULL){
			connector->uploadFile(file_name.toStdString(), file_handle, id_folder_);
		}
	}
	refreshTable();
}

void MainWindow::on_uploadDirectoriesButton_clicked()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "C:", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	std::cout << "name: " << dir.toStdString() << std::endl;

	Connector *connector = &Connector::getInstance();
	connector->uploadDirectory(dir.toStdString(), id_folder_);
}



void MainWindow::closeEvent(QCloseEvent *event){
	std::cout << "close event " << std::endl;
	delete this;
}

void MainWindow::cellClicked(int row, int col)
{
	selected_row_ = row;
	std::cout << "cell at row  " << QString::number(row).toStdString() << " column " << QString::number(col).toStdString() << std::endl;
}