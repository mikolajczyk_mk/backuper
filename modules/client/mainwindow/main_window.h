/**
* @file main_window.h
* @brief Plik nag��wkowy klasy MainWindow.
* @author Krzysztof Kachniarz
*/
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QWidget>
#include <qtablewidget.h>
#include "catalog.h"
#include "file.h"

namespace Ui {
	class MainWindow;
}
/**Klasa odpowiedzialna za wy�wietlanie okna g��wnego programu.*/
class MainWindow : public QWidget
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);

	~MainWindow();


	private slots:
	/**Funkcja obs�uguj�ca zdarzenie wci�ni�cia przycisku "OK".*/
	void on_downloadButton_clicked();
	void cellClicked(int row, int col);
	/**Funkcja obs�uguj�ca zdarzenie wci�ni�cia przycisku "Upload Files".*/
	void on_uploadFilesButton_clicked();
	/**Funkcja obs�uguj�ca zdarzenie wci�ni�cia przycisku "Upload Directory".*/
	void on_uploadDirectoriesButton_clicked();
	/**Funkcja obs�uguj�ca zdarzenie wci�ni�cia przycisku "Delete".*/
	void on_deleteButton_clicked();

private:
	Ui::MainWindow *ui;
	QTableWidget* table_widget;
	long long id_folder_ = 0;
	QStringList table_header;
	std::vector<common::File> files_;
	std::vector<common::Catalog> catalogs_;
	int selected_row_ = -1;
	void refreshTable();
protected:
	void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

};

#endif // MAIN_WINDOW_H
