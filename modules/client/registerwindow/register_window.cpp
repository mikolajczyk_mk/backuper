#include "register_window.h"
#include "ui_register_window.h"
#include "welcome_window.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <md5.h>
#include "connector.h"
#include "main_window.h"

RegisterWindow::RegisterWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RegisterWindow)
{
    ui->setupUi(this);
}

RegisterWindow::~RegisterWindow()
{
    delete ui;
}

void RegisterWindow::on_cancelButton_clicked()
{
	welcomeWindow *_welcomeWindow = new welcomeWindow();
	_welcomeWindow->setWindowTitle("Welcome");
	_welcomeWindow->show();
	this->close();
	delete this;
}

void RegisterWindow::on_okButton_clicked()
{
	QString qnickname = ui->loginLineEdit->text();
	std::string nickname = ui->loginLineEdit->text().toStdString();
	std::string password = common::md5(ui->passwordLineEdit->text().toStdString());
	std::string confirm_password = common::md5(ui->password2LineEdit->text().toStdString());

	if (password != confirm_password){
		std::cout << "passwords are different " << std::endl;
	}
	else{
		Connector* connector = &Connector::getInstance();
		
		int result = connector->logIn(nickname, password, false);
		if (result == 1){
			MainWindow *main_window = new MainWindow();
			main_window->setWindowTitle("Backuper");
			main_window->show();
			this->close();
			delete this;
		}
		else if (result == 0) {
			this->ui->errorLabel->setText("Server disappeard");
		}
		else if (result == -1) {
			this->ui->errorLabel->setText("Login " + qnickname + " already in use");
		}
		
	}
}