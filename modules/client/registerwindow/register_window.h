/**
* @file register_window.h
* @brief Plik nag��wkowy klasy RegisterWindow.
* @author Krzysztof Kachniarz
*/

#ifndef REGISTERWINDOW_H
#define REGISTERWINDOW_H

#include <QWidget>

namespace Ui {
	/**Klasa odpowiedzialna za okienko s�u��ce do rejestracji.*/
class RegisterWindow;
}

class RegisterWindow : public QWidget
{
    Q_OBJECT

public:
    explicit RegisterWindow(QWidget *parent = 0);
    ~RegisterWindow();

private slots:

	/**Metoda s�u��ca do obs�ugi zdarzenia klikni�cia na przycisk "Cancel"*/
	void on_cancelButton_clicked();
	/**Metoda s�u��ca do obs�ugi zdarzenia klikni�cia na przycisk "OK"*/
	void on_okButton_clicked();

private:
    Ui::RegisterWindow *ui;
};

#endif // REGISTERWINDOW_H
