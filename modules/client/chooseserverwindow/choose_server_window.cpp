#include "choose_server_window.h"
#include "ui_choose_server_window.h"
#include "welcome_window.h"
#include "connector.h"
#include <memory>

ChooseServerWindow::ChooseServerWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChooseServerWindow)
{
    ui->setupUi(this);
}

ChooseServerWindow::~ChooseServerWindow()
{
    delete ui;
}


void ChooseServerWindow::on_cancelButton_clicked()
{
	this->close();
}

void ChooseServerWindow::on_okButton_clicked()
{
	QString address = ui->addressLineEdit->text();
	QString port_number = ui->portLineEdit->text();
	std::cout << "port: " << port_number.toInt() << std::endl;

	if (port_number.toInt() <= 0){
		ui->errorLabel->setText("Error! Incorrect port number");
	}
	else {
		Connector* connector = &Connector::getInstance();
		connector->init(address.toStdString(), port_number.toInt());
		if (connector->connect()){
			welcomeWindow *_welcomeWindow = new welcomeWindow();
			_welcomeWindow->setWindowTitle("Welcome");
			_welcomeWindow->show();
			this->close();
		}
		else
		{
			std::string error_message = "Error! Couldnt find " + address.toStdString() + ":" + port_number.toStdString();
			ui->errorLabel->setText(QString::fromUtf8(error_message.c_str()));
		}
	}
}