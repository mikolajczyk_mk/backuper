/**
* @file choose_server_window.h
* @brief  Plik nag��wkowy klasy ChooseServerWindow.
* @author Krzysztof Kachniarz
*/
#ifndef CHOOSESERVERWINDOW_H
#define CHOOSESERVERWINDOW_H

#include <QWidget>

namespace Ui {
class ChooseServerWindow;
}
/**Klasa odpowiedzialna za okienko wy�wietlaj�ce pro�b� o podanie danych potrzebnych do po��czenia z serwerem.*/
class ChooseServerWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ChooseServerWindow(QWidget *parent = 0);
    ~ChooseServerWindow();

private slots:
/**Metoda obs�uguj�ca zdarzenie  klikni�cia przycisku OK. Wywo�uje funkcje inicjuj�c� po��czenie z serwerem.*/
	void on_okButton_clicked();
	/**Metoda obs�uguj�ca zdarzenie klikni�cia przycisku Cancel. Ko�czy dzia�anie programu.*/
	void on_cancelButton_clicked();

private:
    Ui::ChooseServerWindow *ui;
};

#endif // CHOOSESERVERWINDOW_H
