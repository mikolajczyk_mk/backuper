/**
* @file connector.h
* @brief Plik nag��wkowy klasy Connector.
* @author Krzysztof Kachniarz
*/
#ifndef CONNECTOR_H
#define CONNECTOR_H

#include <string>
#include <iostream>
#include <memory>
#include <vector>
#define NOMINMAX
#include "catalog.h"
#include "file.h"


/**Klasa implmenetuj�ca wzorzec singletona s�u��ca do obs�ugi po��cze� po stronie klienckiej.*/
class Connector {
private:
	/**Domy�lny konstruktor*/
	Connector(){}
	Connector(const Connector&) = delete;
	Connector& operator=(const Connector&) = delete;
	/**Metoda s�u��ca do zamykania po��czenia.*/
	static void closeConnection();
	/**Metoda s�u��ca do odbierania informacji na temat katalog�w zgromadzonych na serwerze.
	* @return Zwraca obiekt klasy Catalog z uzupe�nionymi podstawowymi informacjami.
	*/
	static common::Catalog receiveCatalogInfo();
	/**Metoda s�u��ca do odbierania informacji na temat plik�w zgromadzonych na serwerze.
	* @return Zwraca obiekt klasy File z uzupe�nionymi informacjami.
	*/
	static common::File receiveFileInfo();
	/**Metoda sprawdzaj�ca rozmiar pliku.
	* @param file_handle Wska�nik na obiekt klasy FILE wskazuj�cy na plik z kt�rego zostanie pobrany rozmiar.
	* @return Rozmiar pliku.
	*/
	static long long getFileSize(FILE* file_handle);
	
	static int portNumber_;
	static std::string hostAddress_;
	static int socket_;

public:
	/**Metoda zwracaj�ca instancj� singletonu.
	* @return Instancja singletonu.
	*/
	static Connector& getInstance() {
		static Connector instance_;
		return instance_;
	}
	/**Metoda nawi�zuj�ca po��czenie z serwerem.
	* @return Informacja o tym, czy nawi�zanie po��czenia si� uda�o.
	*/
	static int connect();
	/**Metoda uzuep�niaj�ca podstawowe dane na temat hosta, z kt�rym klasa b�dzie si� ��czy�.
	* @param hostname Adres serwera.
	* @param portnumber Numer portu, na kt�rym nas�uchuje serwer.
	*/
	static void init(std::string hostname, int portnumber);
	/**Metoda zwracaj�ca adres serwera.
	* @return Adres serwera.
	*/
	static  std::string getHostAddress();
	/**Metoda wysy�aj�ca do serwera ��danie zalogowania oraz potrzebne dane.
	* @param nickname Login u�ytkownika.
	* @param password Has�o u�ytkownika.
	* @param account Informacja, czy u�ytkownik chce si� zalogowa� na istniej�ce konto (account=true), czy stworzy� nowe (account=false).
	*/
	static int logIn(std::string nickname, std::string password, bool account);
	/**Metoda s�u�aca do wymiany informacji potrzebnej do pobrania pliku. Inicjuje w�a�ciwe pobieranie.
	* @param init Informacja, czy trzeba wysy�a� do serwera ��danie Protocol::SEND_FILE, czy zosta�o ono ju� wcze�niej wys�ane.
	* @param id Identyfikator pliku, kt�ry nale�y pobra�.
	* @param file_handle Wska�nik na obiekt typu FILE okre�laj�cy po�o�enie do kt�rego b�dziemy zapisywa� pobrany plik.
	*/
	static int downloadFile(bool init, long long id, std::FILE* file_handle);
	/**Metoda s�u�aca do wymiany informacji potrzebnej do pobrania katalogu. 
	* @param init Informacja, czy trzeba wysy�a� do serwera ��danie Protocol::SEND_CATALOG, czy zosta�o ono ju� wcze�niej wys�ane.
	* @param id Identyfikator folderu, kt�ry nale�y pobra�.
	* @param path �cie�ka, do kt�rej nale�y zapisa� pobrany plik.
	*/
	static void downloadCatalog(bool init, long long id_catalog, std::string path);
	/**Metoda s�u�aca do wymiany informacji potrzebnej do wys�ania pliku. Inicjuje w�a�ciwe pobieranie.
	* @param name Nazwa pliku.
	* @param file_handle Wska�nik na obiekt FILE wskazuj�cy plik do wys�ania.
	* @param id_folder Identyfikator folderu, do kt�rego u�ytkownik chce wrzuci� plik.
	* @return Informacja o ewentualnych b��dach.
	*/
	static int uploadFile(std::string name, std::FILE *file_handle, long long id_folder);
	/**Metoda s�u�aca do wymiany informacji potrzebnej do wys�ania katalogu. 
	* @param dir �cie�ka do folderu kt�ry u�ytkonik chce wys�a�.
	* @param id_folder Identyfikator folderu, do kt�rego u�ytkownik chce wrzuci� katalog.
	*/
	static void uploadDirectory(std::string dir, long long id_folder);
	/**Metoda wysy�aj�ca ��danie usuni�cia pliku z serwera.
	* @param id_file Identyfkator pliku, kt�ry ma zosta� usuni�ty.
	*/
	static void removeFile(long long id_file);
	/**Metoda wysy�aj�ca ��danie usuni�cia katalogu z serwera.
	* @param id_catalog Identyfkator katalogu, kt�ry ma zosta� usuni�ty.
	*/
	static void removeCatalog(long long id_catalog);
	/**Metoda pobieraj�ca list� plik�w z daneg folderu z serwera.
	* @param id_folder Identyfikator folderu, kt�rego zawarto�� u�ytkownik chce pobra�.
	* @return Wektor plik�w.
	*/
	static std::vector<common::File> getFileList(long long id_folder);
	/**Metoda pobieraj�ca list� katalog�w z daneg folderu z serwera.
	* @param id_folder Identyfikator folderu, kt�rego zawarto�� u�ytkownik chce pobra�.
	* @return Wektor katalog�w.
	*/
	static std::vector<common::Catalog> getCatalogList(long long id_folder);
	~Connector(){};
};

#endif //CONNECTOR_H