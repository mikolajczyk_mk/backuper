#include "connector.h"
#include "sender.h"
#include "receiver.h"
#include <string>
#include <boost/filesystem.hpp>
#include "protocol.h"

#ifdef _WIN32
	#include <winsock2.h>
	#include <windows.h>
	#include <ws2tcpip.h>
	#include <iphlpapi.h>
	#include <stdio.h>
	#include <iostream>
	#include <fstream>
	#include <stdlib.h>
	#pragma comment (lib, "Ws2_32.lib")
	#pragma comment (lib, "Mswsock.lib")
	#pragma comment (lib, "AdvApi32.lib")
#elif __linux__
	#include <netdb.h>
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <zconf.h>
	void closesocket(int socket) { close(socket); }
#endif

#define DEFAULT_BUFLEN 512

using namespace std;
using namespace boost::filesystem;

int Connector::portNumber_ ;
std::string Connector::hostAddress_;
int Connector::socket_;

void Connector::init(std::string host_address, int port_number){
	hostAddress_ = host_address;
	portNumber_ = port_number;
}

int Connector::connect(){
	#ifdef WIN32
		WSADATA wsaData;
		// Initialize Winsock
		int iResult;
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed with error: %d\n", iResult);
			return 0;
		}
	#endif

	int connect_socket;
	const char *sendbuf = "test";
	int recvbuflen = DEFAULT_BUFLEN;

	struct sockaddr_in destination;

	destination.sin_family = AF_INET;
	connect_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (connect_socket < 0)
	{
		printf("\nSocket Creation FAILED!");
		return 0;
	}

	destination.sin_port = htons(portNumber_);
	destination.sin_addr.s_addr = inet_addr(hostAddress_.c_str());

	if (::connect(connect_socket, (struct sockaddr *)&destination, sizeof(destination)) != 0){
		printf("\nSocket Connection FAILED!\n");
		if (connect_socket) closesocket(connect_socket);
		return 0;
	}
	std::cout << "Connected!" << std::endl;
	socket_ = connect_socket;

	return 1;
}

int Connector::logIn(std::string nickname, std::string password, bool account){
	char tmp;
	if (account)
		tmp = Protocol::LOGIN;
	else
		tmp = Protocol::REGISTER;
	char* send_buf = &tmp;
	char recv_buf[DEFAULT_BUFLEN];
	int result;
	send(socket_, send_buf, DEFAULT_BUFLEN, 0);
	result = recv(socket_, recv_buf, DEFAULT_BUFLEN, 0);
	if (result == 0){
		Connector::closeConnection();
		return 0;
	}
	else{
		if (*recv_buf == Protocol::SEND_LOGIN){
			std::cout << "sending login " << std::endl;
			send(socket_, nickname.c_str(), DEFAULT_BUFLEN, 0);
			recv(socket_, recv_buf, DEFAULT_BUFLEN, 0);
			std::cout << "sizeof " << password.c_str();
			if (*recv_buf == Protocol::SEND_PASSWORD){
				send(socket_, password.c_str(), DEFAULT_BUFLEN, 0);
			}
			else
				return -1;

			recv(socket_, recv_buf, DEFAULT_BUFLEN, 0);
			if (*recv_buf == Protocol::ACK){
				std::cout << "Logged in! " << std::endl;
				return 1;
			}
			else if (*recv_buf == Protocol::NACK){
				std::cout << "Incorrect password or login " << std::endl;
				return -1;
			}
		}
		else
			std::cout << "Unknown error" << std::endl;
	}
	return -1;
}

int Connector::uploadFile(std::string name, std::FILE *file_handle, long long id_folder){
	std::replace(name.begin(), name.end(), '\\', '/');
	char tmp = Protocol::UPLOAD_FILE;
	char* send_buf = &tmp;
	char recv_buf[DEFAULT_BUFLEN];
	int result;
	send(socket_, send_buf, DEFAULT_BUFLEN, 0); //UPLOAD_FILE
	result = recv(socket_, recv_buf, DEFAULT_BUFLEN, 0); //send file name
	if (result == 0)
		Connector::closeConnection();
	else if (*recv_buf == Protocol::SEND_FILE_NAME) {
		send(socket_, name.c_str(), DEFAULT_BUFLEN, 0); //send file name

		Sender sender_long = Sender(&socket_); //send catalog id
		sender_long.sendLongLong(id_folder);

		long long filesize = getFileSize(file_handle);
		sender_long.sendLongLong(filesize);//send size

			recv(socket_, recv_buf, DEFAULT_BUFLEN, 0);	//receive ack/nack 
		if (*recv_buf == Protocol::ACK){
			Sender sender = Sender(file_handle, &socket_);
			sender.sendFile();
		}
		else if (*recv_buf == Protocol::NACK){
			
		}
	}	
	return 1;
}

long long Connector::getFileSize(FILE *file_handle){
	fseek(file_handle, 0, SEEK_END);
	long filesize = ftell(file_handle);
	rewind(file_handle);
	return filesize;
}

void Connector::downloadCatalog(bool init, long long main_catalog_id, std::string main_path){
	boost::filesystem::path dir(main_path);
	if (!boost::filesystem::create_directory(dir)) {
		std::cout << "couldnt create file" << std::endl;
	}
	if (init) {
		char tmp = Protocol::DOWNLOAD_CATALOG;
		send(socket_, &tmp, DEFAULT_BUFLEN, 0); //download_catalog
	}
	Sender sender(&socket_);
	Receiver receiver(&socket_);
	sender.sendLongLong(main_catalog_id);
	long long num_of_catalogs;
	receiver.readLongLong(&num_of_catalogs);
	for (int i = 0; i < num_of_catalogs; ++i){
		char recv_buf[DEFAULT_BUFLEN];
		recv(socket_, recv_buf, DEFAULT_BUFLEN, 0);
		std::string path = main_path + "/" + recv_buf;
		long long id_catalog;
		receiver.readLongLong(&id_catalog);
		downloadCatalog(false, id_catalog, path);
	}

	long long num_of_files;
	receiver.readLongLong(&num_of_files);

	for (int i = 0; i < num_of_files; ++i){
		char recv_buf[DEFAULT_BUFLEN];
		std::string file_name;
		std::string file_path;
		long long file_id;
		recv(socket_, recv_buf, DEFAULT_BUFLEN, 0);
		file_name = recv_buf;
		receiver.readLongLong(&file_id);
		file_path = main_path + "/" + file_name;
		FILE* file_handle = fopen(file_path.c_str(), "wb");
		if (file_handle != NULL)
			downloadFile(false, file_id, file_handle);
	}

}

int Connector::downloadFile(bool init, long long id, std::FILE *file_handle){

	if (init) {
		char tmp = Protocol::DOWNLOAD_FILE;
		send(socket_, &tmp, DEFAULT_BUFLEN, 0); //DOWNLOAD_FILE
	}
	char recv_buf[DEFAULT_BUFLEN];
	Sender sender = Sender(&socket_);
	
	sender.sendLongLong(id); //ID
	recv(socket_, recv_buf, DEFAULT_BUFLEN, 0); //ACK/NACK
	if (*recv_buf == Protocol::ACK){
		Receiver receiver(file_handle, &socket_);
		receiver.receiveFile();
		if (file_handle != NULL)
			fclose(file_handle);
	}
	return 1;
}

void Connector::uploadDirectory(std::string dir,long long id_folder_parent){
	std::replace(dir.begin(), dir.end(), '\\', '/');
	path main_dir(dir);

	if (exists(main_dir)){
		if (is_directory(main_dir)){
			typedef std::vector<path> paths;
			paths pth;
			copy(directory_iterator(main_dir), directory_iterator(), back_inserter(pth));
			sort(pth.begin(), pth.end());

			Sender sender = Sender(&socket_);
			Receiver receiver = Receiver(&socket_);
			char tmp = Protocol::UPLOAD_DIRECTORY;
			std::string send_buf = &tmp;
			char recv_buf[DEFAULT_BUFLEN];
			send(socket_, send_buf.c_str(), DEFAULT_BUFLEN, 0); //UPLOAD_DIRECTORY
			send_buf = main_dir.string();
			send(socket_, send_buf.c_str(), DEFAULT_BUFLEN, 0); //dir name
			sender.sendLongLong(id_folder_parent); // send parent folder id
			recv(socket_, recv_buf, DEFAULT_BUFLEN, 0); //ACK/NACK
			long long id_folder;
			receiver.readLongLong(&id_folder);
			if (*recv_buf == Protocol::ACK)
			{
				for (paths::const_iterator it(pth.begin()); it != pth.end(); ++it)
				{
					if (is_directory(*it)){

						uploadDirectory((*it).string(), id_folder);
					}
					else{
						std::cout << *it << std::endl;
						FILE *file_handle = fopen((*it).string().c_str(), "rb");
						uploadFile((*it).string(), file_handle, id_folder);
						fclose(file_handle);
					}

				}
			}
		}
	}

}

void Connector::removeCatalog(long long id_catalog){
	char tmp = Protocol::REMOVE_CATALOG;
	send(socket_, &tmp, DEFAULT_BUFLEN, 0);
	Sender sender(&socket_);
	sender.sendLongLong(id_catalog);
}

void Connector::removeFile(long long id_file){
	char tmp = Protocol::REMOVE_FILE;
	send(socket_, &tmp, DEFAULT_BUFLEN, 0);
	Sender sender(&socket_);
	sender.sendLongLong(id_file);
}

std::vector<common::File> Connector::getFileList(long long id_folder){
	char tmp = Protocol::SHOW_FILES;
	char * show_files_buf = &tmp;
	send(socket_, show_files_buf, DEFAULT_BUFLEN, 0); //SHOW_FILES

	Sender sender_long = Sender(&socket_); //send catalog id
	sender_long.sendLongLong(id_folder);

	long long size_catalogs;
	Receiver receiver_long = Receiver(&socket_); //num of files
	receiver_long.readLongLong(&size_catalogs);

	int size_int = size_catalogs;
	std::vector<common::File> files;
	for (int i = 0; i < size_int; ++i){
		files.push_back(receiveFileInfo());
	}
	return files;
}

common::File Connector::receiveFileInfo(){
	std::string name;
	long long size;
	long long number_of_versions;
	long long id;
	Receiver receiver(&socket_);
	char recv_buf[DEFAULT_BUFLEN];
	recv(socket_, recv_buf, DEFAULT_BUFLEN, 0);
	name = recv_buf;
	receiver.readLongLong(&size);
	receiver.readLongLong(&number_of_versions);
	receiver.readLongLong(&id);
	common::File file = common::File();
	file.setName_(name);
	file.setSize_(size);
	file.setNumberOfVersion_(number_of_versions);
	file.setId_(id);
	return file;
}


std::vector<common::Catalog> Connector::getCatalogList(long long id_folder){
	char tmp = Protocol::SHOW_DIRECTORIES;
	char * show_files_buf = &tmp;
	send(socket_, show_files_buf, DEFAULT_BUFLEN, 0); //SHOW_DIRECTORIES

	Sender sender_long = Sender(&socket_); //send catalog id
	sender_long.sendLongLong(id_folder);

	long long size_catalogs;
	Receiver receiver_long = Receiver(&socket_); //num of files
	receiver_long.readLongLong(&size_catalogs);

	int size_int = size_catalogs;
	std::vector<common::Catalog> catalogs;
	for (int i = 0; i < size_int; ++i){
		catalogs.push_back(receiveCatalogInfo());
	}
	return catalogs;
}

common::Catalog Connector::receiveCatalogInfo(){
	char recv_buf[DEFAULT_BUFLEN];
	common::Catalog catalog;
	std::string name;
	long long catalog_id;
	recv(socket_, recv_buf, DEFAULT_BUFLEN, 0);
	name = recv_buf;
	Receiver receiver(&socket_);
	receiver.readLongLong(&catalog_id);
	catalog.setName_(name);
	catalog.setId_(catalog_id);
	return catalog;
}

void Connector::closeConnection(){
	std::cout << "Connection lost " << std::endl;

#ifdef WIN32
	closesocket(socket_);
	WSACleanup();
#elif __linux__
	close(socket_);
#endif
}
std::string Connector::getHostAddress(){
	return hostAddress_;
}
