#include "welcome_window.h"
#include "ui_welcome_window.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "sign_in_window.h"
#include "register_window.h"
#include "connector.h"
#include <QApplication>


welcomeWindow::welcomeWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::welcomeWindow)
{
    ui->setupUi(this);
}

welcomeWindow::~welcomeWindow()
{
    delete ui;
}

void welcomeWindow::on_SignIn_clicked()
{
	SignInWindow *_signInWindow = new SignInWindow();
	_signInWindow->setWindowTitle("Sign In");
	_signInWindow->show();
	this->close();
	delete this;
}

void welcomeWindow::on_Register_clicked()
{
	RegisterWindow *registerWindow = new RegisterWindow();
	registerWindow->setWindowTitle("Register");
	registerWindow->show();
	this->close();
	delete this;
}
