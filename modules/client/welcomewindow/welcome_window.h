/**
* @file welcome_window.h
* @brief Plik nag��wkowy klasy welcomeWindow.
* @author Krzysztof Kachniarz
*/

#ifndef WELCOMEWINDOW_H
#define WELCOMEWINDOW_H

#include <QMainWindow>
#include "sign_in_window.h"

namespace Ui {
	class welcomeWindow;
}
/**Klasa odpowiedzialna za okienko powitania. Umo�liwia wyb�r rejestracji lub logowania.*/
class welcomeWindow : public QMainWindow
{
    Q_OBJECT

public:
	explicit welcomeWindow(QWidget *parent = 0);
	~welcomeWindow();

private slots:
	/**Metoda odpowiedzialna za obs�ug� zdarzenia klikni�cia przycisku "Sign in", uruchamia okienko logowania.*/
	void on_SignIn_clicked();
	/**Metoda odpowiedzialna za obs�ug� zdarzenia klikni�cia przycisku "Register", uruchamia okienko rejestracji.*/
    void on_Register_clicked();

private:
	Ui::welcomeWindow *ui;
	SignInWindow *signInWindow;
};

#endif // WELCOMEWINDOW_H
